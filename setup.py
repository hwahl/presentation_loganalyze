# -*- coding: utf8-*-
from setuptools import setup, find_packages
import sys
from cx_Freeze import setup, Executable

ver_dic = {}
version_file = open("version.py")
try:
    version_file_contents = version_file.read()
finally:
    version_file.close()

exec(compile(version_file_contents, "version.py", 'exec'), ver_dic)

build_exe_options = {
    "packages": ["os", "argparse"],
     "excludes": ["tkinter"],
    #"compressed": True
}

base = None
options = {"build_exe": build_exe_options}
executables = [Executable("pla.py", base=base)]

setup(
    name="scripts",
    version=ver_dic["VERSION_TEXT"],
    description="process Presentation-logs into meaningful tables.",
    author="Hannes Wahl",
    author_email="hannes.wahl@ukdd.de",
    setup_requires=['setuptools>=18.0', ],
    install_requires=[''],
    packages=find_packages(),
#    entry_points={
#        'console_scripts': ['process_logs=pla'],
#    },
    options= {"build_exe": build_exe_options},
    executables=executables
)
