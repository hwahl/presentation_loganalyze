# Presentation Logfile Analyzer
- it is possible to select different Paradigmas.
- at the moment it is not possible to change the stimulus names via the GUI, only in the script

# How-To

- download/git the whole project
- go to root-dir
- either have a virtualenv setup or directly install with, `--user` for only as the current user
- when on windows and building yourself, please be aware: https://pyinstaller.readthedocs.io/en/latest/usage.html#windows
- don't use UPX on windows 10!
```python
pip install -r reqs.txt
pyinstaller pla.py
pyinstaller pla_win.spec
```
- run appropriate executable

# To Do
- configurable names for signals (Stimuli, Responses, Feedback,...)
- GUI for stimuli selection
- ~~check Dirks Stuff out: https://fusionforge.zih.tu-dresden.de/scm/browser.php?group_id=921&extra=presentation~~
