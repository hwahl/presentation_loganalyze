#! /usr/bin/env python3

# author: Hannes Wahl

class Trial(object):

    def __init__(self, header, stimulus, response=None, time_offset=0):
        """
        initialise Trial, consisting of at least one stimulus (e.g. Picture) and optionally one response (e.g. Buttonpress)
        :param header:
        :param stimulus:
        :param response:
        """
        if isinstance(header, str):
            header = header.split("\t")

        if isinstance(stimulus, str):
            stimulus = stimulus.split("\t")

        self.ntrial = int(stimulus[header.index("Trial")])
        self.stimulus = {"event_type": stimulus[header.index("Event Type")],
                         "code": stimulus[header.index("Code")],
                         "time": int(stimulus[header.index("Time")]),
                         "time_offset": int(stimulus[header.index("Time")]) - time_offset,
                         "ttime": int(stimulus[header.index("TTime")]),
                         "stim_type": stimulus[header.index("Stim Type")],
                         }

        try:
            self.stimulus["subject"] = stimulus[header.index("Subject")]
        except:
            self.stimulus["subject"] = "unkown"

        self._generate_response(response, header)

    def _generate_response(self, response, header):
        if response is not None:
            if isinstance(response, str):
                response = response.split("\t")

            self.response = {"time": int(response[header.index("Time")]),
                             "ttime": int(response[header.index("TTime")]) - self.stimulus["ttime"]}
        else:
            self.response = None

    def return_subject(self):
        return self.stimulus["subject"]

    def return_event_onset_time(self):
        return self.stimulus["time"] / 10000.0

    def return_event_onset_time_off(self):
        return self.stimulus["time_offset"] / 10000.0

    def return_event_code(self):
        return self.stimulus["code"]

    def return_event_type(self):
        return self.stimulus["event_type"]

    def return_stim_type(self):
        return self.stimulus["stim_type"]

    def return_response_time(self):
        try:
            return self.response["ttime"] / 10000.0
        except:
            return -1


class Trial_RewardAnticipation(Trial):

    def __init__(self, header, stimulus, response=None, time_offset=0):
        """
        initialise Trial, consisting of at least one stimulus (e.g. Picture) and optionally one response (e.g. Buttonpress)
        :param header:
        :param stimulus:
        :param response:
        """

        super().__init__(header, stimulus, response=response, time_offset=time_offset)
        self._generate_response(response, header)

    def _generate_response(self, response, header):
        if response is not None:
            if isinstance(response, str):
                response = response.split("\t")

            self.response = {"time": int(response[header.index("Time")]),
                             "ttime": int(response[header.index("TTime")]) - self.stimulus["ttime"],
                             "category": ("good" if self.stimulus[
                                                        "code"] == "UCS" else "bad")}  # this is specifically set for the REWARD-Experiment
        else:
            self.response = None

    def return_response_category(self):
        try:
            return self.response["category"]
        except:
            return "unknown"
