#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
Working through this tutorial: http://zetcode.com/gui/pyqt5/

Icons made by Freepik from www.flaticon.com
"""
import sys
from os import getcwd
from os.path import join
from PyQt5.QtWidgets import (QWidget, QMainWindow, QMessageBox, QAction, QApplication, QMenu, QDesktopWidget, QLabel,
                             QLineEdit, QTextEdit, QGridLayout, QPushButton, QLCDNumber, QVBoxLayout, QSlider,
                             QInputDialog, QFileDialog, QCheckBox, QFrame, QProgressBar, QHBoxLayout, QSplitter)
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtCore import (Qt, pyqtSignal, QObject, QBasicTimer)


class Communicate(QObject):
    closeApp = pyqtSignal()


class Example_Layout(QWidget):
    def __init__(self, mainwindow):
        super().__init__()

        self.initUI(mainwindow)

    def initUI(self, mainwindow):
        title = QLabel('Title')
        author = QLabel('Author')
        review = QLabel('Review')
        dotButton_title = QPushButton("change title")
        dotButton_title.clicked.connect(mainwindow.buttonClicked)
        dotButton_author = QPushButton("change author")
        dotButton_author.clicked.connect(mainwindow.buttonClicked)
        titleEdit = QLineEdit()
        authorEdit = QLineEdit()
        reviewEdit = QTextEdit()

        grid = QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(title, 1, 0)
        grid.addWidget(titleEdit, 1, 1)
        grid.addWidget(dotButton_title, 1, 2)

        grid.addWidget(author, 2, 0)
        grid.addWidget(authorEdit, 2, 1)
        grid.addWidget(dotButton_author, 2, 2)

        grid.addWidget(review, 3, 0)
        grid.addWidget(reviewEdit, 3, 1, 5, 2)

        self.setLayout(grid)


class Example_Signal(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        lcd = QLCDNumber(self)
        sld = QSlider(Qt.Horizontal, self)

        vbox = QVBoxLayout()
        vbox.addWidget(lcd)
        vbox.addWidget(sld)

        self.setLayout(vbox)
        sld.valueChanged.connect(lcd.display)


class Example_Dialog(QWidget):
    def __init__(self, mainwindow):
        super().__init__()

        self.main = mainwindow
        self.initUI()

    def initUI(self):
        self.btn = QPushButton('Dialog', self)
        self.btn.move(20, 20)
        self.btn.clicked.connect(self.showDialog)

        self.le = QLineEdit(self)
        self.le.move(130, 22)

        # self.setGeometry(300, 300, 290, 150)
        # self.setWindowTitle('Input dialog')
        self.show()

    def showDialog(self):
        text, ok = QInputDialog.getText(self, 'Input Dialog',
                                        'Enter your name:')

        if ok:
            self.le.setText(str(text))
            self.main.statusBar().showMessage("Name was changed")


class Example_Mouse(QWidget):

    def __init__(self, mainwindow):
        super().__init__()

        self.initUI(mainwindow)

    def initUI(self, mainwindow):
        grid = QGridLayout()

        x = 0
        y = 0

        self.text = "x: {0},  y: {1}".format(x, y)

        self.label = QLabel(self.text, self)
        grid.addWidget(self.label, 0, 0, Qt.AlignTop)

        self.setMouseTracking(True)

        self.setLayout(grid)

        self.setGeometry(300, 300, 350, 200)
        self.setWindowTitle('Event object')
        self.show()

    def mouseMoveEvent(self, e):
        x = e.x()
        y = e.y()

        text = "x: {0},  y: {1}".format(x, y)
        self.label.setText(text)


class Example_Checkbox(QWidget):

    def __init__(self, mainwindow):
        super().__init__()

        self.main = mainwindow
        self.initUI()

    def initUI(self):

        cb = QCheckBox('Show title', self)
        cb.move(20, 20)
        cb.toggle()
        cb.stateChanged.connect(self.changeTitle)

    def changeTitle(self, state):

        if state == Qt.Checked:
            self.main.setWindowTitle(self.main.title)
        else:
            self.main.setWindowTitle(' ')


class Example_SwitchButtons(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.col = QColor(0, 0, 0)

        redb = QPushButton('Red', self)
        redb.setCheckable(True)
        redb.move(10, 10)

        redb.clicked[bool].connect(self.setColor)

        greenb = QPushButton('Green', self)
        greenb.setCheckable(True)
        greenb.move(10, 60)

        greenb.clicked[bool].connect(self.setColor)

        blueb = QPushButton('Blue', self)
        blueb.setCheckable(True)
        blueb.move(10, 110)

        blueb.clicked[bool].connect(self.setColor)

        self.square = QFrame(self)
        self.square.setGeometry(150, 20, 100, 100)
        self.square.setStyleSheet("QWidget { background-color: %s }" %
                                  self.col.name())

        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('Toggle button')
        self.show()

    def setColor(self, pressed):

        source = self.sender()

        if pressed:
            val = 255
        else:
            val = 0

        if source.text() == "Red":
            self.col.setRed(val)
        elif source.text() == "Green":
            self.col.setGreen(val)
        else:
            self.col.setBlue(val)

        self.square.setStyleSheet("QFrame { background-color: %s }" %
                                  self.col.name())


class Example_Progressbar(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.pbar = QProgressBar(self)
        self.pbar.setGeometry(30, 40, 200, 25)

        self.btn = QPushButton('Start', self)
        self.btn.move(40, 80)
        self.btn.clicked.connect(self.doAction)

        self.timer = QBasicTimer()
        self.step = 0

        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('QProgressBar')
        self.show()

    def timerEvent(self, e):

        if self.step >= 100:
            self.timer.stop()
            self.btn.setText('Finished')
            return

        self.step = self.step + 1
        self.pbar.setValue(self.step)

    def doAction(self):

        if self.timer.isActive():
            self.timer.stop()
            self.btn.setText('Start')
        else:
            self.timer.start(100, self)
            self.btn.setText('Stop')


class Example_QLineEdit(QWidget):

    def __init__(self, mainwindow):
        super().__init__()

        self.main = mainwindow
        self.initUI()

    def initUI(self):
        self.lbl = QLabel(self)
        qle = QLineEdit(self)

        qle.move(60, 100)
        self.lbl.move(60, 40)

        qle.textChanged[str].connect(self.onChanged)

        cb = QCheckBox('Show in statusbar', self)
        cb.move(20, 20)
        cb.toggle()
        cb.stateChanged.connect(self.changeStatusbar)

        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('QLineEdit')
        self.show()

    def changeStatusbar(self, state):

        if state == Qt.Checked:
            self.main.statusBar().showMessage(self.lbl.text())
        else:
            self.main.statusBar().showMessage('bla')

    def onChanged(self, text):
        self.lbl.setText(text)
        self.lbl.adjustSize()


class Button(QPushButton):

    def __init__(self, title, parent):
        super().__init__(title, parent)

        self.setAcceptDrops(True)

    def dragEnterEvent(self, e):

        if e.mimeData().hasFormat('text/plain'):
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):

        self.setText(e.mimeData().text())


class Example_DragDrop(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        edit = QLineEdit('', self)
        edit.setDragEnabled(True)
        edit.move(30, 65)

        button = Button("Button", self)
        button.move(190, 65)

class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.title = 'Main window'
        self.initUI()

    def initUI(self):

        # self.c = Communicate()
        # self.c.closeApp.connect(self.close)

        self.initBars()
        self.initLayout()
        # self.initButtons()

        self.setGeometry(500, 500, 700, 500)
        self.setWindowTitle(self.title)
        self.show()

    def initButtons(self):
        btn1 = QPushButton("Button 1", self)
        btn1.move(30, 50)

        btn2 = QPushButton("Button 2", self)
        btn2.move(150, 50)

        btn1.clicked.connect(self.buttonClicked)
        btn2.clicked.connect(self.buttonClicked)

    def initLayout(self):

        layout = Example_DragDrop()
        self.setCentralWidget(layout)
        # self.textEdit = QTextEdit()
        # self.setCentralWidget(self.textEdit)

        # hbox = QHBoxLayout(self)
        #
        # topleft = FrameButton("Test")
        #
        # topright = QFrame(self)
        # topright.setFrameShape(QFrame.StyledPanel)
        #
        # bottom = QFrame(self)
        # bottom.setFrameShape(QFrame.StyledPanel)
        #
        # splitter1 = QSplitter(Qt.Horizontal)
        # splitter1.addWidget(topleft)
        # splitter1.addWidget(topright)
        #
        # splitter2 = QSplitter(Qt.Vertical)
        # splitter2.addWidget(splitter1)
        # splitter2.addWidget(bottom)
        #
        # hbox.addWidget(splitter2)
        # self.setLayout(hbox)

    def initBars(self):
        self.statusBar().showMessage('Ready')

        menubar = self.menuBar()

        exitAct = QAction(QIcon(join('gfx', 'exit.png')), '&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)

        impMenu = QMenu('Import', self)
        impAct = QAction('Import mail', self)
        impAct.setStatusTip('Import mail from File')
        impMenu.addAction(impAct)

        newAct = QAction('New', self)
        newAct.setStatusTip('Create New File')

        openFile = QAction(QIcon(join('gfx', 'open-folder.png')), 'Open', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open new File')
        openFile.triggered.connect(self.showDialog)

        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(newAct)
        fileMenu.addAction(openFile)
        fileMenu.addMenu(impMenu)
        fileMenu.addAction(exitAct)

        viewMenu = menubar.addMenu('View')

        viewStatAct = QAction('View statusbar', self)
        viewStatAct.setCheckable(True)
        viewStatAct.setStatusTip('View statusbar')
        viewStatAct.setChecked(True)
        viewStatAct.triggered.connect(self.toggleMenu)

        viewMenu.addAction(viewStatAct)

        # self.toolbar = self.addToolBar('Exit')
        # self.toolbar.addAction(exitAct)

    def buttonClicked(self):

        sender = self.sender()
        self.statusBar().showMessage(sender.text() + ' was pressed')

    def toggleMenu(self, state):

        if state:
            self.statusbar.show()
        else:
            self.statusbar.hide()

    def center(self):

        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def closeEvent(self, event):

        reply = QMessageBox.question(self, 'Message',
                                     "Are you sure to quit?", QMessageBox.Yes |
                                     QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def keyPressEvent(self, e):

        if e.key() == Qt.Key_Escape:
            self.close()

    def contextMenuEvent(self, event):

        cmenu = QMenu(self)

        newAct = cmenu.addAction("New")
        opnAct = cmenu.addAction("Open")
        quitAct = cmenu.addAction("Quit")
        action = cmenu.exec_(self.mapToGlobal(event.pos()))

        if action == quitAct:
            self.close()
        elif action == opnAct:
            self.showDialog()

    def showDialog(self):

        fname = QFileDialog.getOpenFileName(self, 'Open file', getcwd())

        if fname[0]:
            f = open(fname[0], 'r')

            with f:
                data = f.read()
                self.textEdit.setText(data)

        self.statusBar().showMessage("File {0} was successfully opened.".format(fname[0]))


from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from PyQt5.Qt import *

import os

class Widget(QWidget):
    """
    https://stackoverflow.com/questions/50283851/how-to-display-list-of-files-in-a-specified-directory-using-pyqt5
    """
    def __init__(self, *args, **kwargs):
        QWidget.__init__(self, *args, **kwargs)

        splitter = QSplitter(Qt.Horizontal)
        hlay = QHBoxLayout(self)
        self.treeview = QTreeView()
        self.listview = QListView()
        splitter.addWidget(self.treeview)
        splitter.addWidget(self.listview)

        hlay.addWidget(splitter)

        path = os.getcwd()

        self.dirModel = QFileSystemModel()
        self.dirModel.setRootPath(QDir.rootPath())
        self.dirModel.setFilter(QDir.NoDotAndDotDot | QDir.AllDirs)

        self.fileModel = QFileSystemModel()
        self.fileModel.setFilter(QDir.NoDotAndDotDot | QDir.Files)

        self.treeview.setModel(self.dirModel)
        self.listview.setModel(self.fileModel)

        self.treeview.setRootIndex(self.dirModel.index(path))
        self.listview.setRootIndex(self.fileModel.index(path))
        self.listview.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.treeview.clicked.connect(self.onClicked_folder)
        self.listview.clicked.connect(self.onClicked_files)

        self.selected_files = []

        buttons = QHBoxLayout(self)
        butt_return = QPushButton("return selected", self)
        butt_return.clicked.connect(self.buttonClicked)

        butt_select = QPushButton("select all", self)
        butt_select.clicked.connect(self.buttonSelectAll)

        buttons.addStretch(1)
        buttons.addWidget(butt_select)
        buttons.addWidget(butt_return)

        vlay = QVBoxLayout(self)
        vlay.addLayout(hlay)
        vlay.addStretch(1)
        vlay.addLayout(buttons)
        self.setLayout(vlay)

    """
    In your slot for the itemSelectionChanged() signal, call selectedItems() to get a QList of QListWidgetItem pointers
    
    http://www.qtcentre.org/threads/56380-PyQT-Listview-index-to-string
    """

    def buttonClicked(self):
        self.return_selectedFiles()

    def onClicked_folder(self, index):
        path = self.dirModel.fileInfo(index).absoluteFilePath()
        self.listview.setRootIndex(self.fileModel.setRootPath(path))

    def onClicked_files(self, index):
        # path =self.fileModel.fileInfo(index).absoluteFilePath()
        # print("Test: {}".format(path))
        # itms = self.listview.selectedIndexes()
        # self.selected_files = []
        # for idx in itms:
        #     self.selected_files.append(self.fileModel.fileInfo(idx).absoluteFilePath())
        # print(self.selected_files)
        pass

    def return_selectedFiles(self):
        itms = self.listview.selectedIndexes()
        self.selected_files = []
        for idx in itms:
            self.selected_files.append(self.fileModel.fileInfo(idx).absoluteFilePath())
        print(self.selected_files)

    def buttonSelectAll(self):
        self.listview.selectAll()
        self.return_selectedFiles()


class AddWidgetWindow(QMainWindow):
    def __init__(self, parent = None):
        super(AddWidgetWindow, self).__init__(parent)

        # main button
        self.addButton = QPushButton('button to add other widgets')
        self.addButton.clicked.connect(self.addWidget)

        # scroll area widget contents - layout
        self.scrollLayout = QFormLayout()

        # scroll area widget contents
        self.scrollWidget = QWidget()
        self.scrollWidget.setLayout(self.scrollLayout)

        # scroll area
        self.scrollArea = QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollWidget)

        # main layout
        self.mainLayout = QVBoxLayout()

        # add all main to the main vLayout
        self.mainLayout.addWidget(self.addButton)
        self.mainLayout.addWidget(self.scrollArea)

        # central widget
        self.centralWidget = QWidget()
        self.centralWidget.setLayout(self.mainLayout)

        # set central widget
        self.setCentralWidget(self.centralWidget)

    def addWidget(self):
        self.scrollLayout.addRow(Test())


class Test(QWidget):
  def __init__( self, parent=None):
      super(Test, self).__init__(parent)

      self.pushButton = QPushButton('I am in Test widget')

      layout = QHBoxLayout()
      layout.addWidget(self.pushButton)
      self.setLayout(layout)
      self.pushButton.clicked.connect(self.deleteLater)


class tabdemo(QTabWidget):
    def __init__(self, parent=None):
        super(tabdemo, self).__init__(parent)
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()

        self.addTab(self.tab1, "Tab 1")
        self.addTab(self.tab2, "Tab 2")
        self.addTab(self.tab3, "Tab 3")
        self.tab1UI()
        self.tab2UI()
        self.tab3UI()
        self.setWindowTitle("tab demo")

    def tab1UI(self):
        layout = QFormLayout()
        layout.addRow("Name", QLineEdit())
        layout.addRow("Address", QLineEdit())
        self.setTabText(0, "Contact Details")
        self.tab1.setLayout(layout)

    def tab2UI(self):
        layout = QFormLayout()
        sex = QHBoxLayout()
        sex.addWidget(QRadioButton("Male"))
        sex.addWidget(QRadioButton("Female"))
        layout.addRow(QLabel("Sex"), sex)
        layout.addRow("Date of Birth", QLineEdit())
        self.setTabText(1, "Personal Details")
        self.tab2.setLayout(layout)

    def tab3UI(self):
        layout = QHBoxLayout()
        layout.addWidget(QLabel("subjects"))
        layout.addWidget(QCheckBox("Physics"))
        layout.addWidget(QCheckBox("Maths"))
        self.setTabText(2, "Education Details")
        self.tab3.setLayout(layout)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    # ex = Example()
    # w = AddWidgetWindow()
    w=tabdemo()
    w.show()
    sys.exit(app.exec_())
