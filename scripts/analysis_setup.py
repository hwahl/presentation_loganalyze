#! env python3

# author: Hannes Wahl

from os import listdir, getcwd
from os.path import join, isfile, exists
from .logfile import Logfile
import json


def run_analysis(logpath, outputdir, json_fpath):
    """
    run analysis on directory containing logfiles with suffix ".log"
    :param logpath: directory path with logfiles
    :param outputdir: where to save the analysis
    :param json_fpath: Path to json-file containing the configuration of the analysis.
    :return:
    """
    print("Current working dir: {}".format(getcwd()))
    print("Working on directory {} ...".format(logpath))

    if exists(json_fpath):
        with open(json_fpath, "r") as f:
            config = json.load(f)
    else:
        config = {"project_name": "ReDO: Task Anticipation Reward",
                  "logfile_suffix": ".log",
                  "offset_event": {"Event Type": "Pulse"},
                  "exclude": [],
                  "onset_stimuli": [
                      # {"Event Type": "Picture", "Code": "CS_win"},
                      # {"Event Type": "Picture", "Code": "CS_verb"},
                      # {"Event Type": "Picture", "Code": "Feedback"},
                      # {"Event Type": "Picture", "Code": "FB.*win"},
                      # {"Event Type": "Picture", "Code": "FB.*verb"}
                  ],
                  "response_stimuli": [
                      {"event": {"Event Type": "Picture", "Code": "[U]?CS.*"},
                       "options": {"only_first": True, "only_same_trial": True,
                                   "check_response_timing": {"response_stimulus_code": "^UCS.*$",
                                                             "clear_onset_list": True}},
                       "output_suffix": "UCS_all"}
                  ],
                  "additional": {"offset_event_count": 0}}

        with open(json_fpath, "w", encoding="utf-8") as f:
            str_ = json.dumps(config, indent=4, sort_keys=True, ensure_ascii=False)
            f.write(str_)
    try:
        suffix = config["logfile_suffix"]
    except:
        suffix = ".log"
    try:
        for f in [f for f in listdir(logpath) if isfile(join(logpath, f)) and f.endswith(suffix)]:
            log = Logfile(join(logpath, f), output_path=outputdir, event_config=config)
            log.write_all_as_csv()
    except NotADirectoryError:
        log = Logfile(logpath, output_path=outputdir, event_config=config)
        log.write_all_as_csv()
    except Exception as e:
        print(e)
    print("Finished processing.")


def main():
    import argparse

    parser = argparse.ArgumentParser(description='Process all Presentation-.log-files in a given directory..')
    parser.add_argument('logdir', help='directory containing all Presentation-.log-files')
    parser.add_argument('outputdir', nargs='?', default=None,
                        help='Output directory for processed logs. Outputname is Logfile-name and according suffixes.')
    parser.add_argument('--json', help="Path to json-file containing the configuration of the analysis.")
    args = parser.parse_args()

    run_analysis(args.logdir, args.outputdir, args.json)


if __name__ == '__main__':
    main()
