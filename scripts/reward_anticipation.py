#! env python3

# author: Hannes Wahl

from os import listdir, getcwd
from os.path import join, isfile, exists
from .logfile import Logfile
import json


def run_analysis(logpath, outputdir, strict_response=True, stimulus_names="", stimulus_types="", feedback_names="", ):
    """
    run analysis on directory containing logfiles with suffix ".log"
    :param logpath: directory path with logfiles
    :param outputdir: where to save the analysis
    :param strict_response: use strict response for REWARD-Experiment
    :return:
    """
    print("Current working dir: {}".format(getcwd()))
    print("Working on directory {} ...".format(logpath))

    # set default values
    # if not stimulus_names:  stimulus_names = ["CS"]
    # if not stimulus_types:  stimulus_types = ["win", "verb"]
    # if not feedback_names: feedback_names = ["Feedback", "FB"]

    json_file = "reward.json"
    if exists(json_file):
        with open(json_file, "r") as f:
            event_config = json.load(f)

    else:
        event_config = {"offset_event": {"Event Type": "Pulse"},
                        "exclude": [],
                        "onset_stimuli": [
                            # {"Event Type": "Picture", "Code": "CS_win"},
                            # {"Event Type": "Picture", "Code": "CS_verb"},
                            # {"Event Type": "Picture", "Code": "Feedback"},
                            # {"Event Type": "Picture", "Code": "FB.*win"},
                            # {"Event Type": "Picture", "Code": "FB.*verb"}
                        ],
                        "response_stimuli": [
                            {"event": {"Event Type": "Picture", "Code": "[U]?CS.*"},
                             "options": {"only_first": True, "only_same_trial": True,
                                         "check_response_timing": {"response_stimulus_code": "^UCS.*$",
                                                                   "clear_onset_list": True}},
                             "output_suffix": "UCS_all"}
                        ],
                        "additional": {"offset_event_count": 0}}

        with open(json_file, "w", encoding="utf-8") as f:
            str_ = json.dumps(event_config, indent=4, sort_keys=True, ensure_ascii=False)
            f.write(str_)

    try:
        for f in [f for f in listdir(logpath) if isfile(join(logpath, f)) and f.endswith(".log")]:
            log = Logfile(join(logpath, f), output_path=outputdir, event_config=event_config)
            log.write_all_as_csv(strict_response)
    except NotADirectoryError:
        log = Logfile(logpath, output_path=outputdir, event_config=event_config)
        log.write_all_as_csv()
    except Exception as e:
        print(e)
    print("Finished processing.")


def main():
    import argparse

    parser = argparse.ArgumentParser(description='Process all Presentation-.log-files in a given directory..')
    parser.add_argument('logdir', help='directory containing all Presentation-.log-files')
    parser.add_argument('outputdir', nargs='?', default=None,
                        help='Output directory for processed logs. Outputname is Logfile-name and according suffixes.')
    parser.add_argument('--strict_response', action='store_true',
                        help="Use strict response categorisation for Reward-analysis.")
    args = parser.parse_args()

    run_analysis(args.logdir, args.outputdir, args.strict_response)


if __name__ == '__main__':
    main()
