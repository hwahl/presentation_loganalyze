#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
Icons made by Freepik from www.flaticon.com
"""

import sys
from .gui.mainwindow import MainWindow
from PyQt5.QtWidgets import QApplication, QStyleFactory


def main():
    app = QApplication(sys.argv)
    app.setStyle(QStyleFactory.create('Fusion'))
    ex = MainWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
