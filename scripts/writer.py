#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl


import os


class Writer(object):
    """
    Writer class for writing results from Presentation-logfile-analysis to .csv or .xlsx
    """

    def __init__(self, path=None, data=None):
        self.valid_suffices = (".csv", ".xls", ".xlsx")
        self.data = data
        self._check_filepath(path)

    def __call__(self, data=None, path=None):
        """
        default writing function: write plain csv
        :return:
        """
        self.write_plain_csv(data=data, path=path)

    def set_path(self, path):
        self._check_filepath(path)

    def _check_filepath(self, path):

        try:
            if path.lower().endswith(self.valid_suffices):
                self.path = path
            elif os.path.isdir(path):
                self.path = os.path.join(path, "results.xls")
            else:
                raise (EnvironmentError("Path: {0} is not recognized as a valid file or directory path.".format(path)))

            self.path = os.path.abspath(self.path)
            self.ext = ("csv" if self.path.lower().endswith(".csv") else "xls")
        except:
            self.path = None

    # def write_with_pandas(self, data):
    #     import pandas as pd
    #
    #     if self.ext == "csv":
    #         pd.DataFrame(data, columns=data.keys()).to_csv(self.path, index=False)
    #     elif self.ext == "xls":
    #         pd.DataFrame(data, columns=data.keys()).to_excel(self.path, index=False)

    def write_plain_csv(self, data=None, path=None):
        """
        write plain csv without pandas
        :param data: list containing dictionaries with column_name:value pairs OR plain list, containing lists
        :return:
        """

        if data is None and self.data is None:
            raise (ValueError, "data is None")
        elif data is not None:
            self.data = data

        if path is None and self.path is None:
            raise(ValueError, "path to write output is None")
        elif path is not None:
            self._check_filepath(path)

        try:
            if isinstance(self.data[0], dict):
                convert = self.convert_dict2lines
            elif isinstance(self.data[0], list):
                convert = self.convert_list2lines
            else:
                raise (ValueError, "data is in unknown format")

            lines = convert(self.data)

            if len(lines) > 1:
                self.check_path_is_csv()
                print("Write response times to file: {}".format(self.path))
                with open(self.path, "w") as f:
                    f.writelines(lines)
        except Exception as e:
            print(e)

    def convert_list2lines(self, data):
        """
        converts list containing lists to lines
        :param data:
        :return: list for file writing
        """
        formatted = ""
        for i in range(len(data[0])):
            # generate formatted string for csv-lines
            formatted += "{" + str(i) + "}\t"
        formatted += "\n"
        lines = []
        for dat in data:
            lines.append(formatted.format(*dat))
        return lines

    def convert_dict2lines(self, data):
        """
        :param data: list containing dictionaries with column_name:value pairs OR plain list, containing lists
        :return: list for file writing
        """
        formatted = ""
        header = ""
        for k in data[0].keys():
            # generate formatted string for csv-lines
            formatted += "{" + k + "}\t"
            header += "{}\t".format(k)
        formatted += "\n"
        header += "\n"

        lines = []
        lines.append(header)
        for dat in data:
            # use formatted string, arguments are column_name:value pairs
            lines.append(formatted.format(**dat))

        return lines

    def check_path_is_csv(self):
        """
        checks if self.path ends with .csv, if not, changes this
        :return:
        """

        if not self.path.lower().endswith(".csv"):
            _, ext = os.path.splitext(self.path)
            self.path = self.path.replace(ext, ".csv")
