#! env python3

# author: Hannes Wahl

from os import listdir, getcwd
from os.path import join, isfile, exists
from .logfile import Logfile_Kosing_neutral_pos, Logfile
import json

def run_analysis(logpath, outputdir, stimulus_names="", stimulus_types="", remove_misses=False):
    """
    run analysis on directory containing logfiles with suffix ".log"
    :param logpath: directory path with logfiles
    :param outputdir: where to save the analysis
    :param strict_response: use strict response for REWARD-Experiment
    :return:
    """
    print("Current working dir: {}".format(getcwd()))
    print("Working on directory {} ...".format(logpath))

    # set default values
    # if not stimulus_names:  stimulus_names = ["stim_event"]
    # if not stimulus_types:  stimulus_types = ["neut", "pos"]

    json_file = "kosing.json"
    try:
        with open(json_file, "r") as f:
            event_config = json.load(f)
    except:
        event_config = {"offset_event": {"Event Type": "Pulse"},
                        "exclude": [{"Event Type": "Pulse"}],
                        "onset_stimuli": [{"Event Type": "Picture", "Code": "stim_event_pos"},
                                          {"Event Type": "Picture", "Code": "stim_event_neut"}],
                        "response_stimuli": [{"event": {"Event Type": "Picture", "Code": "stim_event_pos"},
                                              "options": {"only_first": True, "only_same_trial": True,
                                                          "min_response_time": 0.0},
                                              "output_suffix": "pos"},

                                             {"event": {"Event Type": "Picture", "Code": "stim_event_neut"},
                                             "options": {"only_first": True, "only_same_trial": True,
                                                         "min_response_time": 0.0},
                                              "output_suffix": "neut"}],
                        "additional": []}

        with open(json_file, "w", encoding="utf-8") as f:
            str_ = json.dumps(event_config, indent=4, sort_keys=True, ensure_ascii=False)
            f.write(str_)
    try:
        for f in [f for f in listdir(logpath) if isfile(join(logpath, f)) and f.endswith(".log")]:
            log = Logfile(join(logpath, f), output_path=outputdir, event_config=event_config)
            log.write_all_as_csv(remove_misses)
    except NotADirectoryError:
        log = Logfile(logpath, output_path=outputdir)
        log.write_all_as_csv()
    except Exception as e:
        print(e)
    print("Finished processing.")


def main():
    import argparse

    parser = argparse.ArgumentParser(description='Process all Presentation-.log-files in a given directory..')
    parser.add_argument('logdir', help='directory containing all Presentation-.log-files')
    parser.add_argument('outputdir', nargs='?', default=None,
                        help='Output directory for processed logs. Outputname is Logfile-name and according suffixes.')
    args = parser.parse_args()

    run_analysis(args.logdir, args.outputdir, args.strict_response)


if __name__ == '__main__':
    main()
