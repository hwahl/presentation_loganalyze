#! /usr/bin/env python3

# author: Hannes Wahl

from re import match, sub
from os import makedirs
from os.path import dirname, basename, join, abspath

from .presentation_nic.paradigm.presentation import Presentation

from .writer import Writer


def listAND(x, y):
    """
    do a "bitwise" AND-comparison of both lists, see: https://stackoverflow.com/questions/32192163/python-and-operator-on-two-boolean-lists-how
    :param x: boolean list
    :param y: boolean list
    :return:  and-compared list
    """
    return [a and b for a, b in zip(x, y)]


class Logfile(object):

    def __init__(self, file_path, output_path=None, event_config=None):
        """

        :param file_path:
        :param output_path:
        :param stimulus_names:
        :param stimulus_types:
        :param offset_event: Name of Event Type which first appearence marks the time offset for logfile
        """
        self.logfile_path = file_path
        if not output_path or output_path is None:
            output_path = dirname(file_path)

        try:
            makedirs(output_path)
        except:
            pass

        from .trial import Trial
        self.trial_object = Trial

        self.output_path = output_path

        self.time_offset_s = 0.0

        self.setup_events(event_config)
        #
        # if stimulus_names is None or stimulus_types is None:
        #     raise (TypeError("Stimulus names, stimulus types is None. Abort Mission."))
        # self.stimulus_names = stimulus_names
        # self.stimulus_types = stimulus_types
        #
        # self.offset_event = offset_event

        # self.output_files = {}
        # self.header = []
        self.trials = {}

    def setup_events(self, event_config):
        """
        setup event-configuration.

        :param event_config: dict with list of interesting events, e.g. event_config["exclude]=[{"Event Type": "Pulse", "Code": "30"}]
                                                                        event_config["response_stimuli"]=[{"Event Type": "Picture", "Code": "stim_event_neut"}, {"Event Type": "Picture", "Code": "stim_event_pos"}]
        :return:
        """
        self.events = {}

        if event_config is None:
            event_config = {"offset_event": {"Event Type": "Pulse"},
                            "exclude": [{"Event Type": "Pulse"}],
                            "onset_stimuli": [{"Event Type": ".*"}],
                            "response_stimuli": {"events": [{"Event Type": ".*"}],
                                                 "options": {"only_first": True, "only_same_trial": True,
                                                             "min_response_time": 0.0}},
                            "additional": []}
        # events for configuring the log before extracting onset and response times
        self.events["offset_event"] = event_config["offset_event"]
        self.events["exclude"] = event_config["exclude"]

        # events from whom onsets and responses will be extracted
        self.events["onset_stimuli"] = event_config["onset_stimuli"]
        self.events["response_stimuli"] = event_config["response_stimuli"]
        self.events["additional"] = event_config["additional"]

    def get_trials(self):
        """
        get all trials for logfile, but use Presentation-Module from Dirk Müller
        :return:
        """
        self.pres = Presentation(self.logfile_path)

        # get time offset, if set
        if self.events["offset_event"]:
            # get filter_vector with offset_event-Type, first element will be the offset
            filter_time_offset = self.pres.filter_time_corrected_presentation(self.events["offset_event"])
            try:
                self.time_offset_s = self.pres.get_onset_list({"Time"}, filter_time_offset)[0][0]
                # generate time corrected presentation
                indices_offset_event = [i for i, x in enumerate(filter_time_offset) if x]
                try:
                    index_offset_event = indices_offset_event[self.events["additional"]["offset_event_count"]]
                except:
                    index_offset_event = indices_offset_event[0]
                self.pres.set_time_corrected_presentation(index_offset_event)
            except IndexError:
                print("Timeoffset for Event {0} could not be found.".format(self.events["offset_event"]))
            except Exception as e:
                print(e)

        # get lines to exclude and store filter
        self.filter_exclude_lines = [True] * len(self.pres.get_time_corrected_presentation())
        for exclude in self.events["exclude"]:
            self.filter_exclude_lines = listAND(self.filter_exclude_lines,
                                                self.pres.filter_time_corrected_presentation(exclude,
                                                                                             invert_filter=True))

    def get_trials_legacy(self):
        """
        get all trials for Logfile, eliminating any 'Pulses' from the MRI-scanner
        :return:
        """

        # open file, read all lines
        with open(self.logfile_path, "r") as f:
            lines = f.readlines()

        # get header
        self.header = lines[3].split("\t")

        # get first line with "Pulse" --> time offset for fMRI-Analysis and starting point for analysis
        starting_line = 5
        time_offset = 0
        for s in lines[starting_line:]:
            if "Pulse" in s:
                time_offset = int(s.split("\t")[self.header.index("Time")])
                break
            else:
                starting_line += 1

        # time_offset = int([s.split("\t") for s in lines[5:] if "Pulse" in s][0][self.header.index("Time")])

        # get only the lines without "Pulse" in it
        pulse_free = []
        for s in lines[starting_line:]:
            if "Quit" in s:
                pulse_free.append(s.split("\t"))
                break
            if "Pulse" not in s:
                pulse_free.append(s.split("\t"))

        # pulse_free = [s.split("\t") for s in lines[starting_line:] if "Pulse" not in s and not "\n" in s]
        trial_idx = []
        try:
            for t in pulse_free:
                trial_idx.append(t[self.header.index("Trial")])
        except Exception as e:
            # most like end of file is reached so quit
            pass

        # generate dictionary with trials
        idx = 0
        try:
            while idx < len(pulse_free):
                # get line
                line = pulse_free[idx]
                # get trial number
                ntrial = line[self.header.index("Trial")]
                # find the number of associated trials with this line
                indices = [index for index, value in enumerate(trial_idx) if value == ntrial]
                # convert to integer for easier access in trials-dictionary
                ntrial = int(ntrial)
                if len(indices) is 1:
                    # dismiss line if only the response in Trial -- do nothing with it
                    if line[self.header.index("Event Type")] != "Response":
                        # generate Trial and concatenate dictionaries
                        self.trials = {**self.trials,
                                       **{ntrial: self.trial_object(header=self.header, stimulus=line,
                                                                    time_offset=time_offset)}}
                else:
                    line0 = pulse_free[indices[0]]
                    line1 = pulse_free[indices[1]]
                    # todo: multiple responses in one trial
                    # sort elements in Trials in correct order
                    if line0[self.header.index("Event Type")] == "Picture":
                        stimulus_line = line0
                        response_line = line1

                    else:
                        stimulus_line = line1
                        response_line = line0

                    self.trials = {**self.trials,
                                   **{ntrial: self.trial_object(header=self.header, stimulus=stimulus_line,
                                                                response=response_line, time_offset=time_offset)}}
                idx += len(indices)
        except Exception as e:
            print(e)

    def write_all_as_csv(self, some_boolean=False):
        """
        if trials were not setup, get all trials, then write all necessary files
        :param some_boolean: can be used for anything, just a wildcard parameter
        :return:
        """
        if not self.trials:
            self.get_trials()
        try:
            self.write_times_as_csv()
        except Exception as e:
            print(e)

    def write_times_as_csv(self):
        writer = Writer()

        # write onsets to csv
        for event in self.events["onset_stimuli"]:
            filter_onset = self.pres.filter_time_corrected_presentation(event)
            data = self.pres.get_onset_list(self.pres.columns_descriptions,
                                            listAND(filter_onset, self.filter_exclude_lines), description=True)
            # generate filename, replace any special characters with _, multiple will also be replaced by one _
            file_path = abspath(join(self.output_path,
                                     basename(self.logfile_path).replace(".log",
                                                                         "_onset_{0}.csv".format(sub(r'\W{1,}', '_',
                                                                                               event[
                                                                                                   "Code"])))))
            if data:
                writer.write_plain_csv(data=data, path=file_path)
        try:
            # write response times to csv
            for response_stimulus in self.events["response_stimuli"]:
                event = response_stimulus["event"]
                options = response_stimulus["options"]
                output_suffix = (
                    response_stimulus["output_suffix"] if response_stimulus["output_suffix"] else event["Code"])
                # get filter vector for event
                filter_event = self.pres.filter_time_corrected_presentation(event)
                # get filter vector for responses
                filter_response = self.pres.filter_time_corrected_presentation({"Event Type": "Response"})
                # get response times with options set in event_config
                data = self.pres.get_response_times(
                    filter_vector_event=listAND(filter_event, self.filter_exclude_lines),
                    filter_vector_response=listAND(filter_response,
                                                   self.filter_exclude_lines),
                    **options)
                # generate filename, replace any special characters with _, multiple will also be replaced by one _
                file_path = abspath(join(self.output_path,
                                         basename(self.logfile_path).replace(".log",
                                                                             "_response_{0}.csv".format(sub(r'\W{1,}', '_',
                                                                                                   output_suffix)))))
                if data:
                    writer.write_plain_csv(data=data, path=file_path)
        except Exception as e:
            print(e)
        # write additional stuff to csv (not implemented yet)
        for add in self.events["additional"]:
            pass

    def write_response_time_as_csv_legacy(self, some_boolean=False):
        """
        write response times, diff between picture shown and response triggered, in regards to stimuli, f. ex. CS_win --> UCS_win to csv, tab-separated
        :param strict_response: if true, will change response_category, if response was triggered directly after CS_* stimulus
        :return:
        """
        for stimulus_name in self.stimulus_names:
            for stimulus_type in self.stimulus_types:
                data = []
                for trial in self.trials.values():
                    if match("{0}.*{1}".format(stimulus_name, stimulus_type), trial.return_event_code()):
                        data.append(
                            {
                                "Subject": trial.return_subject(),
                                "TrialNumber": trial.ntrial,
                                "Event_Type": trial.return_event_type(),
                                "Code": trial.return_event_code(),
                                "OnsetTime": trial.return_event_onset_time(),
                                "OnsetTime_corrected": trial.return_event_onset_time_off(),
                                "ResponseTime": trial.return_response_time(),
                                "Stim_Type": trial.return_stim_type()
                            })
                if data:
                    file_path = join(self.output_path,
                                     basename(self.logfile_path).replace(".log", "_{0}_{1}.csv".format(
                                         stimulus_name, stimulus_type)))

                    writer = Writer(file_path, data)
                    writer()


class Logfile_RewardAnticipation(Logfile):

    def __init__(self, file_path, output_path=None, stimulus_names=None, stimulus_types=None,
                 feedback_names=None, strict_response=False):
        """

        :param file_path:
        :param output_path:
        :param stimulus_names:
        :param stimulus_types:
        """
        super().__init__(file_path, output_path=output_path, stimulus_names=stimulus_names,
                         stimulus_types=stimulus_types)

        if feedback_names is None:
            raise (TypeError("feedback names is None. Abort Mission."))
        self.feedback_names = feedback_names
        self.strict_response = strict_response

        from .trial import Trial_RewardAnticipation as Trial
        self.trial_object = Trial

    def write_all_as_csv(self, strict_response=False):
        """
        if trials were not setup, get all trials, then write all necessary files
        :param strict_response: only for response times and REWARD-experiment
        :return:
        """
        if not self.trials:
            self.get_trials()
        self.write_stimuli_as_csv()
        self.write_feedback_as_csv()
        self.write_response_time_as_csv(strict_response)

    def write_stimuli_as_csv(self):
        """
        write onset times for stimuli, for example CS_win and CS_verb, into csv, tab-separated
        :return:
        """
        for stimulus_name in self.stimulus_names:
            for stimulus_type in self.stimulus_types:
                file_path = join(self.output_path,
                                 basename(self.logfile_path).replace(".log", "_{0}_{1}.csv".format(
                                     stimulus_name, stimulus_type)))
                print("Write Stimulus onset times to file: {}".format(file_path))
                with open(file_path, "w") as f:
                    f.write("TrialNumber\tEvent_Type\tCode\tOnset\n")
                    for trial in self.trials.values():
                        if stimulus_name in trial.return_event_code() and \
                                stimulus_type in trial.return_event_code():
                            line = "{ntrial}\t{type}\t{code}\t{onset:f}\n".format(ntrial=trial.ntrial,
                                                                                  type=trial.return_event_type(),
                                                                                  code=trial.return_event_code(),
                                                                                  onset=trial.return_event_onset_time())
                            f.write(line)

    def write_feedback_as_csv(self):
        """
        write Feedback onset times for stimuli, for example CS_win --> Feedback_win, into csv, tab-separated
        :return:
        """
        for feedback_name in self.feedback_names:
            for stimulus_type in self.stimulus_types:
                file_path = join(self.output_path,
                                 basename(self.logfile_path).replace(".log", "_{0}_{1}.csv".format(
                                     feedback_name, stimulus_type)))
                print("Write Feedback onset times to file: {}".format(file_path))
                line = ""
                with open(file_path, "w") as f:
                    f.write("TrialNumber\tEvent_Type\tCode\tOnset\n")
                    for trial in self.trials.values():
                        if feedback_name in trial.return_event_code() and \
                                stimulus_type in self.trials[trial.ntrial - 2].return_event_code():
                            subtype = self.trials[
                                trial.ntrial - 2].return_event_code().replace(self.stimulus_names[0], "")
                            line = "{ntrial}\t{type}\t{code}\t{onset:f}\n".format(ntrial=trial.ntrial,
                                                                                  type=trial.return_event_type(),
                                                                                  code=trial.return_event_code() + subtype,
                                                                                  onset=trial.return_event_onset_time())
                            f.write(line)
                if not line:
                    import os
                    os.remove(file_path)

    def write_response_time_as_csv(self, strict_response=False):
        """
        write response times, diff between picture shown and response triggered, in regards to stimuli, f. ex. CS_win --> UCS_win to csv, tab-separated
        :param strict_response: if true, will change response_category, if response was triggered directly after CS_* stimulus
        :return:
        """
        for stimulus_type in self.stimulus_types:
            header = "Subject\tTrialNumber\tEvent_Type\tCode\tOnsetTime\tOnsetTime_corrected\tResponseTime\tStim Type\tCategory\n"
            lines = []
            for trial in self.trials.values():
                if trial.return_event_code() == "UCS" and \
                        stimulus_type in self.trials[trial.ntrial - 1].return_event_code():
                    # if the stimulus before the response inducing stimulus also has a response, count response as bad, for strict_response
                    if strict_response and self.trials[trial.ntrial - 1].response is not None:
                        category = "bad"
                    else:
                        category = trial.return_response_category()

                    lines.append(
                        "{subject}\t{ntrial}\t{type}\t{code}\t{onset:f}\t{onset_corr:f}\t{responsetime:f}\t{stim_type}\t{category}\n".format(
                            subject=trial.return_subject(),
                            ntrial=trial.ntrial,
                            type=trial.return_event_type(),
                            code=trial.return_event_code(),
                            onset=trial.return_event_onset_time(),
                            onset_corr=trial.return_event_onset_time_off(),
                            responsetime=trial.return_response_time(),
                            stim_type=trial.return_stim_type(),
                            category=category))
            if lines:
                file_path = join(self.output_path,
                                 basename(self.logfile_path).replace(".log",
                                                                     "_UCS_{}.csv".format(stimulus_type)))
                print("Write response times to file: {}".format(file_path))
                with open(file_path, "w") as f:
                    f.write(header)
                    for line in lines:
                        f.write(line)


class Logfile_Kosing_neutral_pos(Logfile):

    def __init__(self, file_path, output_path=None, event_config=None):
        """

        :param file_path:
        :param output_path:
        :param stimulus_names:
        :param stimulus_types:
        """
        super().__init__(file_path, output_path=output_path, event_config=event_config)

    def write_response_time_as_csv(self, remove_misses=False):
        """
        write response times, diff between picture shown and response triggered, in regards to stimuli, f. ex. CS_win --> UCS_win to csv, tab-separated
        :param remove_misses: if true, will discard all rows, where "Stim Type" is "miss" or "incorrect"
        :return:
        """
        for stimulus_name in self.stimulus_names:
            for stimulus_type in self.stimulus_types:
                header = "Subject\tTrialNumber\tEvent_Type\tCode\tOnsetTime\tOnsetTime_corrected\tResponseTime\tStim Type\n"
                lines = []
                for trial in self.trials.values():
                    if trial.return_stim_type() == "miss":
                        print("test")

                    if (match("{0}.*{1}".format(stimulus_name, stimulus_type), trial.return_event_code())
                            and not (match("(miss|incorrect)",
                                           trial.return_stim_type()) and remove_misses)):
                        lines.append(
                            "{subject}\t{ntrial}\t{type}\t{code}\t{onset:f}\t{onset_corr:f}\t{responsetime:f}\t{stim_type}\n".format(
                                subject=trial.return_subject(),
                                ntrial=trial.ntrial,
                                type=trial.return_event_type(),
                                code=trial.return_event_code(),
                                onset=trial.return_event_onset_time(),
                                onset_corr=trial.return_event_onset_time_off(),
                                responsetime=trial.return_response_time(),
                                stim_type=trial.return_stim_type()))
                if lines:
                    file_path = join(self.output_path,
                                     basename(self.logfile_path).replace(".log", "_{}_{}.csv".format(
                                         stimulus_name, stimulus_type)))
                    print("Write response times to file: {}".format(file_path))
                    with open(file_path, "w") as f:
                        f.write(header)
                        for line in lines:
                            f.write(line)
