#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
Icons made by Freepik from www.flaticon.com
"""

import shutil
import json
from os import getcwd, listdir, remove
from os.path import join, isfile

from PyQt5.QtWidgets import (QLabel, QVBoxLayout, QLineEdit, QPushButton, QWidget, QTextEdit, QHBoxLayout,
                             QGridLayout, QComboBox, QFileDialog, QCheckBox, QFormLayout, QGroupBox, QSizePolicy)
from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import QTextCursor


# TODO JSON-Fileselector

class ParadigmaWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent
        hbox = QHBoxLayout()
        self.combo = QComboBox(self)

        self.updateParadigmaCombobox()

        btn_addJson = QPushButton("+")
        btn_addJson.clicked.connect(self.selectJsonFile)

        # cb_strictResponse = QCheckBox("Be Strict with Response times?", self)
        # cb_strictResponse.stateChanged.connect(self.setStrictResponse)
        #
        # cb_removeMisses = QCheckBox("Remove misses and incorrect responses?", self)
        # cb_removeMisses.stateChanged.connect(self.setRemoveMisses)
        #
        hbox.setSpacing(50)
        hbox.addWidget(self.combo)
        hbox.addWidget(btn_addJson)
        # vbox.addWidget(cb_strictResponse)
        # vbox.addWidget(cb_removeMisses)

        # self.cboxes = {"reward_anticipation": cb_strictResponse,
        #                "kosing_neutral_pos": cb_removeMisses}
        #
        # for widget in self.cboxes.values():
        #     widget.setHidden(True)

        self.formGroupBox = QGroupBox("Select Paradigma type")
        layout = QVBoxLayout()
        layout.addItem(hbox)
        self.formGroupBox.setLayout(layout)
        _layout = QVBoxLayout()
        _layout.addWidget(self.formGroupBox)
        self.setLayout(_layout)

    def onActivated(self, text):
        self.parent.parameters["paradigm"] = self.parent.available_paradigma[text]
        # self.showCheckbox(self.parent.available_paradigma[text])
        self.parent.statusBar().showMessage("Paradigma {} selected.".format(text))

    def selectJsonFile(self):
        json_dir = join(getcwd(), 'json')
        fname, _ = QFileDialog.getOpenFileName(self, 'Open additional json file', getcwd())
        # self.qle_input.setText(fname)
        # self.parent.parameters["logdir"] = fname
        try:
            if fname:
                shutil.copy2(fname, json_dir)
                self.updateParadigmaCombobox()
                self.parent.statusBar().showMessage("Path {0} was successfully added.".format(fname))
        except Exception as e:
            print(e)

    def updateParadigmaCombobox(self):

        json_dir = join(getcwd(), 'json')
        for fp in [join(json_dir, f) for f in listdir(json_dir) if isfile(join(json_dir, f)) and f.endswith('.json')]:
            with open(fp, 'r') as f:
                tmp = json.load(f)
            try:
                self.parent.available_paradigma[tmp["project_name"]] = fp
            except KeyError:
                self.parent.available_paradigma[fp] = fp

        self.combo.clear()
        for key in self.parent.available_paradigma:
            self.combo.addItem(key)
        self.combo.activated[str].connect(self.onActivated)
    # def setStrictResponse(self, state):
    #     self.parent.parameters["strict_response"] = (state == Qt.Checked)
    #
    # def setRemoveMisses(self, state):
    #     self.parent.parameters["remove_misses"] = (state == Qt.Checked)

    # def showCheckbox(self, currentIndex):
    #     """
    #     show only checkbox associated to choice in combobox
    #     :param currentIndex:
    #     :return:
    #     """
    #     try:
    #         for key in self.cboxes:
    #             widget = self.cboxes[key]
    #             if key == currentIndex:
    #                 widget.setHidden(False)
    #             else:
    #                 widget.setHidden(True)
    #     except Exception as e:
    #         print(e)


class LogTextEdit(QTextEdit):
    def __init__(self):
        super().__init__()

    def onUpdateText(self, text):
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertText(text)
        self.setTextCursor(cursor)
        self.ensureCursorVisible()


class LogWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        self.formGroupBox = QGroupBox("Log")

        layout = QVBoxLayout()
        self.parent.log = LogTextEdit()
        self.parent.log.setReadOnly(True)
        self.parent.log.setMinimumWidth(400)

        layout.addWidget(self.parent.log)
        self.formGroupBox.setLayout(layout)
        _layout = QVBoxLayout()
        _layout.addWidget(self.formGroupBox)
        self.setLayout(_layout)


class RunWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        btn_input = QPushButton("Run Analysis")
        btn_input.clicked.connect(self.parent.runAnalysis)
        _layout = QVBoxLayout()
        _layout.addWidget(btn_input)
        self.setLayout(_layout)


class LineEditDroppable(QLineEdit):
    def __init__(self, updateDict, fieldname):
        super().__init__()

        self.setAcceptDrops(True)
        self.field = updateDict
        self.fieldname = fieldname

        self.textChanged[str].connect(self.updateField)

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        url = e.mimeData().urls()[0]
        self.setText(url.toLocalFile())
        # self.updateField[self.fieldname] = url.toLocalFile()

    def updateField(self, url):
        self.field[self.fieldname] = url


class PathWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.parent = parent

        lbl_input = QLabel('Input Dir')
        lbl_output = QLabel('Output Dir')
        self.qle_input = LineEditDroppable(self.parent.parameters, "logdir")
        self.qle_output = LineEditDroppable(self.parent.parameters, "outputdir")
        btn_input = QPushButton("...")
        btn_input.clicked.connect(self.selectFolderInput)
        btn_output = QPushButton("...")
        btn_output.clicked.connect(self.selectFolderOutput)

        lbl_json = QLabel('Paradigma Config')
        self.combo = QComboBox(self)
        self.combo.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.updateParadigmaCombobox()

        btn_addJson = QPushButton("+")
        btn_addJson.clicked.connect(self.addJsonFile)

        btn_remJson = QPushButton("-")
        btn_remJson.clicked.connect(self.removeJsonFile)

        hbox_json = QHBoxLayout()
        hbox_json.setSpacing(10)
        hbox_json.addWidget(self.combo)
        hbox_json.addWidget(btn_addJson)
        hbox_json.addWidget(btn_remJson)

        hbox_input = QHBoxLayout()
        hbox_input.setSpacing(10)
        hbox_input.addWidget(self.qle_input)
        hbox_input.addWidget(btn_input)
        hbox_output = QHBoxLayout()
        hbox_output.setSpacing(10)
        hbox_output.addWidget(self.qle_output)
        hbox_output.addWidget(btn_output)
        layout = QFormLayout()
        layout.setSpacing(20)

        layout.addRow(lbl_input, hbox_input)
        layout.addRow(lbl_output, hbox_output)
        layout.addRow(lbl_json, hbox_json)

        self.formGroupBox = QGroupBox("Select paths")
        vbox1 = QVBoxLayout()
        vbox1.addItem(layout)
        self.formGroupBox.setLayout(vbox1)
        vbox2 = QVBoxLayout()
        vbox2.addWidget(self.formGroupBox)
        self.setLayout(vbox2)

    def selectFolderInput(self):
        fname = QFileDialog.getExistingDirectory(self, 'Open directory', getcwd())
        self.qle_input.setText(fname)
        self.parent.parameters["logdir"] = fname
        self.parent.statusBar().showMessage("Path {0} was successfully added.".format(fname))

    def selectFolderOutput(self):
        fname = QFileDialog.getExistingDirectory(self, 'Open directory', getcwd())
        self.qle_output.setText(fname)
        self.parent.parameters["outputdir"] = fname
        self.parent.statusBar().showMessage("Path {0} was successfully added.".format(fname))

    def onActivated(self, text):
        self.parent.parameters["paradigm"] = self.parent.available_paradigma[text]
        # self.showCheckbox(self.parent.available_paradigma[text])
        self.parent.statusBar().showMessage("Paradigma {} selected.".format(text))

    def addJsonFile(self):
        self.selectJsonFile('add')

    def removeJsonFile(self):
        self.selectJsonFile('remove')

    def selectJsonFile(self, action):
        json_dir = join(getcwd(), 'json')
        title = "{0} json file".format(action)
        fname, _ = QFileDialog.getOpenFileName(self, title, json_dir)

        if fname:
            try:
                if action == 'add':
                    shutil.copy2(fname, json_dir)
                elif action == 'remove':
                    remove(fname)
            except Exception as e:
                print(e)
            self.updateParadigmaCombobox()
            self.parent.statusBar().showMessage(
                "Path {0} was successfully {1}ed.".format(fname, action.replace('e', '')))

    def updateParadigmaCombobox(self):
        json_dir = join(getcwd(), 'json')
        # reset available pardigmas
        self.parent.available_paradigma = {"---": None, }
        for fp in [join(json_dir, f) for f in listdir(json_dir) if isfile(join(json_dir, f)) and f.endswith('.json')]:
            with open(fp, 'r') as f:
                tmp = json.load(f)
            try:
                self.parent.available_paradigma[tmp["project_name"]] = fp
            except KeyError:
                self.parent.available_paradigma[fp] = fp

        self.combo.clear()
        for key in self.parent.available_paradigma:
            self.combo.addItem(key)
        self.combo.activated[str].connect(self.onActivated)
