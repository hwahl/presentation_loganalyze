#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl
"""
Icons made by Freepik from www.flaticon.com
"""

import sys
from os.path import join, isfile

from PyQt5.QtWidgets import (QVBoxLayout, QTextEdit, QMainWindow, QWidget,
                             QMessageBox, QAction, QHBoxLayout, QProgressBar)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt, QObject, pyqtSignal

from .widgets import PathWidget, ParadigmaWidget, RunWidget, LogWidget
from .EventConfigurator import EventConfiguratorTabWidget


class Stream(QObject):
    """
    https://stackoverflow.com/questions/44432276/print-out-python-console-output-to-qtextedit
    """
    newText = pyqtSignal(str)

    def write(self, text):
        self.newText.emit(str(text))


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.title = 'Presentation Log Analyzer'

        self.parameters = {"logdir": "", "outputdir": "", "strict_response": False, "paradigm": "",
                           "remove_misses": False}
        # if any changes are made here, also check ParadigmaWidget!
        self.available_paradigma = {"---": None, }
        # "ReDO: Task Anticipation Reward": "reward_anticipation",
        # "Kosing: Neutral-Pos": "kosing_neutral_pos"}
        self.initUI()

        sys.stdout = Stream(newText=self.log.onUpdateText)

    def __del__(self):
        sys.stdout = sys.__stdout__

    def initUI(self):

        self.setMinimumSize(800, 600)

        self.initBars()
        self.initLayout()

        self.setGeometry(500, 500, 800, 640)
        self.setWindowTitle(self.title)
        self.show()

    def initLayout(self):

        _widget = QWidget()
        _layout1 = QVBoxLayout()
        self.pathselector = PathWidget(self)
        # self.paradigma = ParadigmaWidget(self)
        self.runwidget = RunWidget(self)
        self.logwidget = LogWidget(self)
        # self.eventconfig = EventConfiguratorTabWidget(self)

        # self.paradigma.setMinimumWidth(380)

        _layout1.addWidget(self.pathselector)
        # _layout1.addWidget(self.paradigma)
        # _layout1.addWidget(self.eventconfig)
        _layout1.addStretch(1)
        _layout1.addWidget(self.runwidget)

        _layout2 = QVBoxLayout(_widget)
        _layout2.addLayout(_layout1)
        _layout2.addWidget(self.logwidget)
        self.setCentralWidget(_widget)

    def initBars(self):

        self.statusBar().showMessage('Ready')

        # self.progressBar = QProgressBar()
        # self.statusBar().addPermanentWidget(self.progressBar)
        #
        # # This is simply to show the bar
        # self.progressBar.setGeometry(15, 20, 100, 25)
        # self.progressBar.setValue(50)

        menubar = self.menuBar()

        exitAct = QAction(QIcon(join('gfx', 'exit.png')), '&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)

        showPars = QAction('Show Parameters', self)
        showPars.setStatusTip('Show current parameters.')
        showPars.triggered.connect(self.showParameters)

        fileMenu = menubar.addMenu('&Menu')
        fileMenu.addAction(showPars)
        fileMenu.addAction(exitAct)

    def closeEvent(self, event):

        reply = QMessageBox.question(self, 'Message',
                                     "Are you sure to quit?", QMessageBox.Yes |
                                     QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            try:
                self.dialog.close()
            except:
                pass
            event.accept()
        else:
            event.ignore()

    def keyPressEvent(self, e):

        if e.key() == Qt.Key_Escape:
            self.close()

    def runAnalysis(self):
        self.log.setText("")
        if self.parameters["paradigm"] is None or self.parameters["paradigm"] is "":
            self.statusBar().showMessage("No paradigm selected!")

        elif isfile(self.parameters["paradigm"]):
            from ..analysis_setup import run_analysis
            run_analysis(logpath=self.parameters["logdir"],
                         outputdir=self.parameters["outputdir"],
                         json_fpath=self.parameters["paradigm"])

        # elif self.parameters["paradigm"] == "reward_anticipation":
        #     from ..reward_anticipation import run_analysis
        #     run_analysis(logpath=self.parameters["logdir"],
        #                  outputdir=self.parameters["outputdir"],
        #                  strict_response=self.parameters["strict_response"])
        #
        # elif self.parameters["paradigm"] == "kosing_neutral_pos":
        #     from ..Kosing_neutral_pos import run_analysis
        #     run_analysis(logpath=self.parameters["logdir"],
        #                  outputdir=self.parameters["outputdir"],
        #                  remove_misses=self.parameters["remove_misses"])

    def showParameters(self):
        self.dialog = Second("{}".format(self.parameters))

        self.dialog.show()


class Second(QMainWindow):
    def __init__(self, text=""):
        super().__init__()
        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)
        self.textEdit.setText(text)
