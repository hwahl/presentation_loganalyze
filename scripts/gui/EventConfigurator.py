#!/usr/bin/python3
# -*- coding: utf-8 -*-
# author: Hannes Wahl

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *


class EventConfiguratorTabWidget(QTabWidget):
    def __init__(self, parent=None):
        super(EventConfiguratorTabWidget, self).__init__(parent)

        self.parent = parent

        self.tab0 = QWidget()
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.tab3 = QWidget()

        self.addTab(self.tab0, "Response")
        self.addTab(self.tab1, "Offset")
        self.addTab(self.tab2, "Exclude")
        self.addTab(self.tab3, "Onset")

        self.tab0UI()
        self.tab1UI()
        self.tab2UI()
        self.tab3UI()

        self.setGeometry(500, 500, 800, 600)
        self.show()

    def tab0UI(self):
        self.formGroupBox = QGroupBox("Response")
        layout = QVBoxLayout()
        e = ResponseConfigWidget(self.parent)
        layout.addWidget(e)
        self.formGroupBox.setLayout(layout)
        _layout = QVBoxLayout()
        _layout.addWidget(self.formGroupBox)
        self.tab0.setLayout(_layout)

    def tab1UI(self):
        layout = QFormLayout()
        layout.addRow("Name", QLineEdit())
        layout.addRow("Address", QLineEdit())
        self.tab1.setLayout(layout)

    def tab2UI(self):
        layout = QFormLayout()
        sex = QHBoxLayout()
        sex.addWidget(QRadioButton("Male"))
        sex.addWidget(QRadioButton("Female"))
        layout.addRow(QLabel("Sex"), sex)
        layout.addRow("Date of Birth", QLineEdit())
        self.tab2.setLayout(layout)

    def tab3UI(self):
        layout = QHBoxLayout()
        layout.addWidget(QLabel("subjects"))
        layout.addWidget(QCheckBox("Physics"))
        layout.addWidget(QCheckBox("Maths"))
        self.tab3.setLayout(layout)


class EventConfigWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.parent = parent

        self.initWidget()

    def initWidget(self):
        self.initEvent()
        self.setLayout(self.lyt_events)

    def initEvent(self):
        self.lyt_events = QVBoxLayout(self)
        self.lyt_events.addLayout(self.addEvent())
        # self.lyt_events.addStretch(1)

    def addEvent_qle(self):
        lyt_event = QGridLayout(self)
        lbl_parameter = QLabel("Parameter")
        lbl_filter = QLabel("Filter")
        le_parameter = QLineEdit()
        le_filter = QLineEdit()

        btn_addremove = QPushButton( "+")
        btn_addremove.clicked.connect(self.addRemoveClicked)

        lyt_event.addWidget(lbl_parameter, 0, 0)
        lyt_event.addWidget(lbl_filter, 1, 0)
        lyt_event.addWidget(le_parameter, 0, 1)
        lyt_event.addWidget(le_filter, 1, 1)
        lyt_event.addWidget(btn_addremove, 1, 2)

        return lyt_event

    def addEvent(self):
        lyt_event = QFormLayout(self)
        lbl_event_type = QLabel("Event Type")
        lbl_code = QLabel("Code")
        lbl_stim_type = QLabel("Stim Type")
        le_code = QLineEdit(self)
        le_stim_type = QLineEdit(self)
        combo_event_type = QComboBox(self)
        for key in ["Picture", "Pulse", "Response", "Sound"]:
            combo_event_type.addItem(key)
        combo_event_type.activated[str].connect(self.setEventType)

        # btn_addremove = QPushButton( "+")
        # btn_addremove.clicked.connect(self.addRemoveClicked)

        lyt_event.addRow(lbl_event_type, combo_event_type)
        lyt_event.addRow(lbl_code, le_code)
        lyt_event.addRow(lbl_stim_type, le_stim_type)

        return lyt_event

    def setEventType(self):
        pass

    def addRemoveClicked(self):
        pass

    def addEventOnClick(self):
        pass

class AddRemoveButton(QPushButton):
    def __init__(self, parent=None):
        super().__init__(self, parent)
        self.clicked.connect(self.deleteLater)


class ResponseTimingWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        lyt_form = QFormLayout()
        lbl_code = QLabel("Response Stimuli Code")
        le_code = QLineEdit()

        lyt_form.addRow(lbl_code, le_code)

        chb_clear_onset_list = QCheckBox("Clear Onset List")

        lyt_vbox = QVBoxLayout(self)
        lyt_vbox.addLayout(lyt_form)
        lyt_vbox.addWidget(chb_clear_onset_list)
        # lyt_vbox.addStretch(1)

        self.setLayout(lyt_vbox)

class ResponseConfigWidget(EventConfigWidget):

    def __init__(self, parent=None):
        super(ResponseConfigWidget, self).__init__(parent)

    def initWidget(self):
        # self.initEvent()
        # self.initOptions()

        lyt = QVBoxLayout(self)
        lyt.addLayout(self.addEvent())
        lyt.addLayout(self.initOptions())
        lyt.addStretch(1)
        self.setLayout(lyt)

    def initOptions(self):
        self.lyt_options = QVBoxLayout(self)
        hbox = QHBoxLayout(self)
        chb_only_first = QCheckBox("Count only first response")
        chb_only_first.stateChanged.connect(self.setOnlyFirst)
        chb_only_same_trial = QCheckBox("Count only on same trial")
        chb_only_same_trial.stateChanged.connect(self.setOnlySameTrial)
        chb_check_response_timing = QCheckBox("Check Response Timing")
        chb_check_response_timing.stateChanged.connect(self.switchResponseTiming)

        self.response_widget = ResponseTimingWidget(self.parent)
        self.response_widget.setHidden(True)

        hbox.addWidget(chb_only_first)
        hbox.addWidget(chb_only_same_trial)
        hbox.addWidget(chb_check_response_timing)

        self.lyt_options.addLayout(hbox)
        self.lyt_options.addWidget(self.response_widget)
        # self.lyt_options.addStretch(1)

        self.options = {"only_first": chb_only_first,
                        "only_same_trial": chb_only_same_trial,
                        "check_response_timing": self.response_widget}

        return self.lyt_options

    def switchResponseTiming(self, state):
        self.response_widget.setHidden(state != Qt.Checked)

    def setOnlyFirst(self, state):
        pass

    def setOnlySameTrial(self, state):
        pass

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    # ex = Example()
    # w = AddWidgetWindow()
    w = EventConfiguratorTabWidget()
    w.show()
    sys.exit(app.exec_())
