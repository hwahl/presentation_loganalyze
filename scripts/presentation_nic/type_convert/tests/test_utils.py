#!/usr/bin/env python3
"""Test converstion to another type."""


import math
import type_convert.utils
import unittest


class TestUtils(unittest.TestCase):

    """Test all functions in type_convert.utils."""

    def setUp(self):
        """set up a converter and initialize the divisor."""
        self._convert = type_convert.utils.Float(divisor=1.0)

    def test_convert_to_float(self):
        """Test converting to float."""
        self.assertEqual(self._convert.convert_to_float(5.4), 5.4)
        self.assertEqual(self._convert.convert_to_float('5.4'), 5.4)
        self.assertEqual(self._convert.convert_to_float(5), 5)  # This is odd
        self.assertNotEqual(self._convert.convert_to_float(12345678901234567890), 12345678901234567890)
        self.assertEqual(self._convert.convert_to_float(12345678901234567890), 12345678901234567890.0)
        self.assertEqual(self._convert.convert_to_float('12345678901234567890'), 12345678901234567890.0)
        self._convert.set_divisor(1000.0)
        self.assertEqual(self._convert.convert_to_float('12345678901234567890'), 12345678901234567890.0 / 1000.0)

    def test_convert_to_float_na_str(self):
        """Test converting to flaot, na, str."""
        self.assertEqual(self._convert.convert_to_float_na_str('5'), 5.0)
        self.assertEqual(self._convert.convert_to_float_na_str('5.9'), 5.9)
        self.assertEqual(self._convert.convert_to_float_na_str('na'), None)
        self.assertTrue(math.isnan(self._convert.convert_to_float_na_str('nan')))
        self.assertEqual(self._convert.convert_to_float_na_str('5,900'), '5,900')
        self.assertEqual(self._convert.convert_to_float_na_str('something'), 'something')

    def tearDown(self):
        """Nothing to do."""
        pass
