#!/usr/bin/env python3
"""Convert to another type."""


from __future__ import absolute_import, print_function
from __future__ import division


class Float(object):

    """Try to convert to flot."""

    def __init__(self, divisor=1.0):
        """Inititialize a divisor."""
        self._divisor = divisor

    def set_divisor(self, divisor):
        """Set divisor."""
        self._divisor = divisor
        return self.get_divisor

    def get_divisor(self):
        """Get divisor."""
        return self._divisor

    def convert_to_float(self, value):
        """Value must bei convertible to float.

        value will be divided by divisor.
        """
        return float(value) / self._divisor

    def convert_to_float_na_str(self, value):
        """value must convertible to float, or string 'na' or to string.

        value will be divided by divisor, if value is convertible to float.
        A string 'na' will return None.
        Everything else will return its string representation.
        """
        try:
            return float(value) / self._divisor
        except ValueError:
            if value.lower() == 'na':
                return None
            else:
                return str(value)
