#!/usr/bin/env python3
"""
presentation.py.

Read presentation log file and convert it to a python dictionary.
"""

import numpy
import paradigm.presentation
import sys
import tempfile
import time
import unittest


class TestPresentation(unittest.TestCase):

    """Class for testing the parsing of presentation log files."""

    @staticmethod
    def set_up_tmp_file(tempfile_handler, file_content):
        """Set up a temporary file for testing."""
        if sys.version_info.major >= 3:
            content = bytes(file_content, 'UTF-8')
        else:
            content = file_content
        tempfile_handler.file.write(content)
        tempfile_handler.file.flush()
        return(tempfile_handler)

    def setUp(self):
        """Set up a fake presentation file for testing."""
        self._temp_file = self.set_up_tmp_file(tempfile.NamedTemporaryFile(), file_contents)
        self._presentation = paradigm.presentation.Presentation(self._temp_file.name)
        self._temp_file_error1 = self.set_up_tmp_file(tempfile.NamedTemporaryFile(), file_contents_error1)
        self._temp_file_error2 = self.set_up_tmp_file(tempfile.NamedTemporaryFile(), file_contents_error2)
        self._temp_file_error3 = self.set_up_tmp_file(tempfile.NamedTemporaryFile(), file_contents_error3)
        self._temp_file_short = self.set_up_tmp_file(tempfile.NamedTemporaryFile(), file_contents_short_event)
        self._temp_file_long = self.set_up_tmp_file(tempfile.NamedTemporaryFile(), file_contents_long_event)

    def test_file_validity(self):
        """Test, if erroneous log files raising exception."""
        self.assertRaises(TypeError, paradigm.presentation.Presentation, self._temp_file_error1.name)
        self.assertRaises(TypeError, paradigm.presentation.Presentation, self._temp_file_error2.name)
        self.assertRaises(TypeError, paradigm.presentation.Presentation, self._temp_file_error3.name)

    def test_file_contents(self):
        """Test, if we loaded the file correctly."""
        file_handler = open(self._temp_file.name, 'r')
        first_line = file_handler.readlines()[0]
        self.assertEqual(first_line, 'Scenario - PANT_links\n')

    def test_get_presentation(self):
        """test get_presentation."""
        self.assertEqual(self._presentation.scenario, 'PANT_links')
        # self.assertEqual(self._presentation.date, '03.05.2014')
        self.assertEqual(self._presentation.time_str, '16:48:52')
        self.assertEqual(self._presentation.date_str, '2014.05.03')
        self.assertEqual(self._presentation.start_time, time.mktime((2014, 5, 3, 16, 48, 52, 0, 0, 0)))
        self.assertEqual(time.tzname[0], 'CET')
        column_description = ['Subject', 'Trial', 'Event Type', 'Code', 'Time', 'TTime', 'TTime Uncertainty',
                              'Duration', 'Duration Uncertainty', 'ReqTime', 'ReqDur', 'Stim Type', 'Pair Index']
        self.assertEqual(self._presentation.columns_descriptions, column_description)
        self.assertEqual(self._presentation.get_presentation()[0]['Time'], -9668.0 / 10000.0)
        self.assertEqual(self._presentation.get_presentation()[2]['Code'], 'Pulse_wait')
        self.assertIsNone(self._presentation.get_presentation()[3]['TTime Uncertainty'])

    def test_get_time_corrected_presentation(self):
        """test get_time_corrected_presentation, sets time of n-th event to 0 and corrects all events accordingly."""
        self.assertEqual(self._presentation.get_time_corrected_presentation()[0]['Time'], 0.0)
        self.assertAlmostEqual(self._presentation.get_time_corrected_presentation()[1]['Time'], 918578.0 / 10000.0)
        self._presentation.set_time_corrected_presentation(1)
        self.assertEqual(self._presentation.get_time_corrected_presentation()[1]['Time'], 0.0)

    def test_set_time_corrected_presentation(self):
        """test get_time_corrected_presentation, sets time of n-th event to 0 and corrects all events accordingly."""
        for index in range(10):
            self.assertEqual(self._presentation.set_time_corrected_presentation(index)[index]['Time'], 0.0)

    def test_filter_time_corrected_presentation(self):
        """test filter_time_corrected_presentation."""
        result_array = numpy.zeros(34)
        result_array[0] = True
        filter_reg_exp = {'Event Type': '^Picture$', 'Code': '^instruction$'}
        self.assertTrue(numpy.array_equal(self._presentation.filter_time_corrected_presentation(filter_reg_exp), result_array))
        result_array[:] = False
        result_array[16] = True
        result_array[25] = True
        filter_reg_exp = {'Stim Type': '^hit$'}
        self.assertTrue(numpy.array_equal(self._presentation.filter_time_corrected_presentation(filter_reg_exp), result_array))
        filter_reg_exp = {'Stim Typ': '^hit$'}
        self.assertRaises(ValueError, self._presentation.filter_time_corrected_presentation, filter_reg_exp)
        result_array[:] = True
        filter_reg_exp = {}
        self.assertTrue(numpy.array_equal(self._presentation.filter_time_corrected_presentation(filter_reg_exp), result_array))

    def test_get_filtered_onset(self):
        """test, if we can get some onsets using predefined filters."""
        filter_reg_exp = {'Stim Type': '^hit$'}
        filter_vector = self._presentation.filter_time_corrected_presentation(filter_reg_exp)
        condition = 'hit'
        duration = None
        tmod = None
        pmod = None
        events = self._presentation.get_time_corrected_presentation()
        onset = self._presentation.get_filtered_onset(condition=condition, duration=duration, filter_vector=filter_vector,
                                                      pmod=pmod, tmod=tmod, events=events)
        target = {'hit': {'name': 'hit', 'onset': [161.81, 176.8471], 'duration': [0.1658, 0.1491], 'pmod': [None, None], 'tmod': [None, None]}}
        self.assertEqual(onset, target)
        # filter_reg_exp_1 = {'Stim Type': '^hit$'}
        # filter_vector_1 = self._presentation.filter_time_corrected_presentation(filter_reg_exp_1)
        # filter_reg_exp_2 = {'Code': '^safe$'}
        # filter_vector_2 = self._presentation.filter_time_corrected_presentation(filter_reg_exp_2)
        # events = self._presentation.get_time_corrected_presentation()
        # conditions = ['hit', 'safe']
        # filter_vectors = [filter_vector_1, filter_vector_2]
        # tmod = [None, None]
        # pmod = [None, None]
        # durations = [None, None]
        # targets = [{'name': ['hit'], 'onsets': [1.0, 2.0], 'duration': [0.1658, 0.400], 'pmod': None, 'tmod': None},
        #            {'name': ['safe'], 'onsets': [1.0, 2.0], 'duration': [0.1658, 0.400], 'pmod': None, 'tmod': None}]
        # actuals = self._presentation.get_filtered_onset(conditions=conditions, events=events, filter_vectors=filter_vectors,
        #                                                  tmod=tmod, pmod=pmod, durations=durations)
        # for target, actual in zip(targets, actuals):
        #     self.assertDictWithAlmostEqual(target, actual)

    def test_get_onset_list(self):
        """Test, if we can get a list of our onsets."""
        columns_descriptions = ['']
        filter_vector = numpy.zeros(34)
        filter_vector[16] = True
        filter_vector[25] = True
        self.assertRaises(ValueError, self._presentation.get_onset_list, columns_descriptions=columns_descriptions, filter_vector=filter_vector)
        columns_descriptions = []
        self.assertEqual(self._presentation.get_onset_list(columns_descriptions=columns_descriptions, filter_vector=filter_vector), [])
        columns_descriptions = ['Duration']
        result = [[1658.0 / 10000.0], [1491.0 / 10000.0]]
        self.assertEqual(self._presentation.get_onset_list(columns_descriptions=columns_descriptions, filter_vector=filter_vector), result)
        columns_descriptions = ['Duration', 'Code']
        result = [[1658.0 / 10000.0, 'rechts'], [1491.0 / 10000.0, 'links']]
        self.assertEqual(self._presentation.get_onset_list(columns_descriptions=columns_descriptions, filter_vector=filter_vector), result)
        result = [['Duration', 'Code'], [1658.0 / 10000.0, 'rechts'], [1491.0 / 10000.0, 'links']]
        self.assertEqual(self._presentation.get_onset_list(columns_descriptions=columns_descriptions, filter_vector=filter_vector, description=True), result)
        # 025_400011_1	4	Picture	rechts	1608432	132013	9	1658	10	132000	1500	hit	18
        # 025_400011_1	5	Picture	links	1758803	131968	1	1491	5	132000	1500	hit	27

    def assertDictWithAlmostEqual(self, targets, actuals):
        """Assert with a dictionary containg floats."""
        self.assertTrue(len(targets) == len(actuals))
        for target, actual in zip(targets, actuals):
            self.assertEqual(target.keys(), actual.keys())
            for target_key, target_val in target.items():
                if isinstance(actual[target_key], float):
                    self.assertAlmostEqual(target_val, actual[target_key])
                else:
                    self.assertEqual(target_val, actual[target_key])
        return True

    def test_get_response_time(self):
        """Test, if response following event is correctly calculated."""
        filter_reg_exp_event = {'Stim Type': '^hit$'}
        filter_reg_exp_response = {'Event Type': '^Response$'}
        filter_event = self._presentation.filter_time_corrected_presentation(filter_reg_exp_event)
        filter_response = self._presentation.filter_time_corrected_presentation(filter_reg_exp_response)
        targets = [{'name': 'response', 'onset': 161.81, 'duration': 0.1658, 'response_time': 0.6456, 'response_number': 1},
                   {'name': 'response', 'onset': 176.8471, 'duration': 0.1491, 'response_time': 0.3481, 'response_number': 1},
                   {'name': 'response', 'onset': 176.8471, 'duration': 0.1491, 'response_time': 0.3482, 'response_number': 2}]
        actuals = self._presentation.get_response_times(event_name='response', only_first=False,
                                                        filter_vector_event=filter_event,
                                                        filter_vector_response=filter_response)
        # print(targets, '\n\n\n', actuals, 'finished\n\n\n')
        self.assertDictWithAlmostEqual(targets, actuals)
        actuals = self._presentation.get_response_times(event_name='response',
                                                        filter_vector_event=filter_event,
                                                        filter_vector_response=filter_response)
        self.assertDictWithAlmostEqual(targets[0:2], actuals)
        actuals = self._presentation.get_response_times(event_name='response', min_response_time=0.4,
                                                        filter_vector_event=filter_event,
                                                        filter_vector_response=filter_response)
        self.assertDictWithAlmostEqual(targets[0:1], actuals)
        actuals = self._presentation.get_response_times(event_name='response', max_response_time=0.4,
                                                        filter_vector_event=filter_event,
                                                        filter_vector_response=filter_response)
        self.assertDictWithAlmostEqual(targets[1:2], actuals)

    def test_get_paradim_duration(self):
        """Test, if we know the length of our paradigm."""
        presentation = paradigm.presentation.Presentation(self._temp_file_short.name)
        self.assertAlmostEquals(presentation.get_paradigm_duration(), 100.0)
        presentation = paradigm.presentation.Presentation(self._temp_file_long.name)
        self.assertAlmostEquals(presentation.get_paradigm_duration(), 200.0)

    def tearDown(self):
        """Cleaning up fake presentation file."""
        self._temp_file.close()


file_contents_error1 = """Scenari - PANT_links
Logfile written - 05/03/2014 16:48:52

Subject	Trial	Event Type	Code	Time	TTime	Uncertainty	Duration	Uncertainty	ReqTime	ReqDur	Stim Type	Pair Index
"""
file_contents_error2 = """Scenario - PANT_links
Logfil written - 05/03/2014 16:48:52

Subject	Trial	Event Type	Code	Time	TTime	Uncertainty	Duration	Uncertainty	ReqTime	ReqDur	Stim Type	Pair Index
"""
file_contents_error3 = """Scenario - PANT_links
Logfile written - 05/03/2014 16:48:52

Subjec	Trial	Event Type	Code	Time	TTime	Uncertainty	Duration	Uncertainty	ReqTime	ReqDur	Stim Type	Pair Index
"""
file_contents_short_event = """Scenario - PANT_links
Logfile written - 05/03/2014 16:48:52

Subject	Trial	Event Type	Code	Time	TTime	Uncertainty	Duration	Uncertainty	ReqTime	ReqDur	Stim Type	Pair Index

025_400011_1	1	Picture	instruction	-10000	0	0	10000	2	0	next	other	0
025_400011_1	1	Picture	Pulse_wait	0	10000	0	10000	8	0	next	other	0
025_400011_1	2	Response	1	990000	0	2
"""
file_contents_long_event = """Scenario - PANT_links
Logfile written - 05/03/2014 16:48:52

Subject	Trial	Event Type	Code	Time	TTime	Uncertainty	Duration	Uncertainty	ReqTime	ReqDur	Stim Type	Pair Index

025_400011_1	1	Picture	instruction	-10000	0	0	10000	2	0	next	other	0
025_400011_1	1	Picture	Pulse_wait	0	10000	0	1990000	8	0	next	other	0
025_400011_1	2	Response	1	990000	0	2
"""
file_contents = """Scenario - PANT_links
Logfile written - 05/03/2014 16:48:52

Subject	Trial	Event Type	Code	Time	TTime	Uncertainty	Duration	Uncertainty	ReqTime	ReqDur	Stim Type	Pair Index

025_400011_1	1	Picture	instruction	-9668	0	1	918636	2	0	next	other	0
025_400011_1	1	Response	1	908910	918578	2
025_400011_1	2	Picture	Pulse_wait	908968	0	1	543122	8	0	next	other	0
025_400011_1	3	Pulse	100	1350326	441357	NA
025_400011_1	3	Pulse	100	1375725	466756	NA
025_400011_1	3	Pulse	100	1401124	492155	NA
025_400011_1	3	Pulse	100	1426523	517554	NA
025_400011_1	3	Pulse	100	1451923	542954	NA
025_400011_1	3	Picture	Pulse_5	1452090	543121	7	24208	8	0	24100	other	0
025_400011_1	4	Picture	safe	1476419	0	212	542	214	0	500	other	0
025_400011_1	4	Pulse	100	1477322	903	NA
025_400011_1	4	Pulse	100	1502721	26302	NA
025_400011_1	4	Pulse	100	1528120	51701	NA
025_400011_1	4	Pulse	100	1553519	77100	NA
025_400011_1	4	Pulse	100	1578919	102500	NA
025_400011_1	4	Pulse	100	1604318	127899	NA
025_400011_1	4	Picture	rechts	1608432	132013	9	1658	10	132000	1500	hit	18
025_400011_1	4	Response	3	1614888	138469	1
025_400011_1	5	Picture	safe	1626835	0	1	497	2	0	500	other	0
025_400011_1	5	Pulse	100	1629717	2882	NA
025_400011_1	5	Pulse	100	1655116	28281	NA
025_400011_1	5	Pulse	100	1680515	53680	NA
025_400011_1	5	Pulse	100	1705915	79080	NA
025_400011_1	5	Pulse	100	1731314	104479	NA
025_400011_1	5	Pulse	100	1756713	129878	NA
025_400011_1	5	Picture	links	1758803	131968	1	1491	5	132000	1500	hit	27
025_400011_1	5	Response	2	1762284	135449	3
025_400011_1	5	Response	2	1762285	135450	3
025_400011_1	6	Picture	danger	1777040	0	1	497	2	0	500	other	0
025_400011_1	6	Pulse	100	1782112	5072	NA
025_400011_1	6	Pulse	100	1807511	30471	NA
025_400011_1	6	Pulse	100	1832911	55871	NA
025_400011_1	6	Pulse	100	1858310	81270	NA
025_400011_1	6	Pulse	100	1883709	106679	NA
"""
