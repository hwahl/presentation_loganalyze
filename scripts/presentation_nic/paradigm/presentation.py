#!/usr/bin/env python3

# author: Dirk Müller
# author: Hannes Wahl
"""
presentation.py.

Read presentation log file and convert it to a python dictionary.
"""

from __future__ import absolute_import, print_function
from __future__ import division
import copy
# import numpy
import scripts.presentation_nic.type_convert.utils as utils
import re
import sys
import time

if sys.version_info.major >= 3:
    from itertools import zip_longest as zip_longest  # noqa
else:
    from itertools import izip_longest as zip_longest


class Presentation(object):
    """Read Presentation file and create a Python dictionary."""

    _type_float_na_str = ('TTime Uncertainty', 'ReqDur')  # NA for external trigger, or float, or str
    _type_str = ('Subject', 'Event Type', 'Code', 'Stim Type', 'Pulse')  # NA for external trigger, or float, or str
    _type_float = ('Time', 'TTime', 'Duration', 'Duration Uncertainty', 'ReqTime')  # Allways float
    _type_int = ('Trial', 'Pair Index')  # int

    def __init__(self, presentation_file):
        """Init."""
        self._presentation_file = presentation_file
        self.scenario = None
        self.date_str = None
        self.time_str = None
        self.start_time = None
        self.end_time = None
        self.columns_descriptions = None
        self._onset_list = []
        self._time_corrected_onset_list = []
        self._time_corrected_index = None
        self._log_content = None
        converter = utils.Float(divisor=10000.0)
        self._switch_case_conversion = dict.fromkeys(self._type_float_na_str, converter.convert_to_float_na_str)
        self._switch_case_conversion.update(dict.fromkeys(self._type_float, converter.convert_to_float))
        self._switch_case_conversion.update(dict.fromkeys(self._type_str, str))
        self._switch_case_conversion.update(dict.fromkeys(self._type_int, int))
        self._parse_presentation_log()

    def _test_paradigm_type(self):
        """Test, if we are using a presentation log file."""
        length = len(self._log_content)
        if length < 5:
            raise TypeError("Presentation log file must have at least 5 lines, "
                            "found only {}.".format(length))
        if not self._log_content[0].startswith('Scenario -'):
            raise TypeError("Presentation log file must start with 'Scenario -"
                            "'")
        if not self._log_content[1].startswith('Logfile written - '):
            raise TypeError("'Logfile written - ' must be in the second line "
                            "of a presentation log file.")
        for i in range(3, length):
            if (self._log_content[i].strip() == '' or
                    self._log_content[i].startswith('#')):
                continue
            choice_a = 'Subject\tTrial\tEvent Type\tCode'
            choice_b = 'Subject\tPulse\tEvent Type\tCode'
            choice_c = 'Pulse\tEvent Type\tCode\tTime'
            choice_d = 'Trial\tEvent Type\tCode'
            if (not self._log_content[i].startswith(choice_a) and
                    not self._log_content[i].startswith(choice_b) and
                    not self._log_content[i].startswith(choice_c) and
                    not self._log_content[i].startswith(choice_d)):
                raise TypeError(
                    "\n'{ca}' or \n'{cb}'\n or \n{cc}\n  or \n{cd}\nmust be the beginning "
                    "of the fourth or a following line of a presentation log "
                    "file, we found: \n'{l}' in line {n}.".format(
                        ca=choice_a, cb=choice_b, cc=choice_c, cd=choice_d,
                        l=self._log_content[i], n=i + 1))
            return i + 2
        return 5

    def _parse_line(self, line, line_number):
        """Parse a single line of the log file.

        :line: line to parse.
        :line_number: line number in presentation file.
        :return: exit_loop, value_dict
        """
        value_dict = {}
        try:
            if line.strip() == '':
                return False, None
            if 'Event Type' in line:
                return True, value_dict  # ignore everything for new value dict.
            for description, value in zip_longest(
                    self.columns_descriptions, line.split('\t')):
                if (description in value_dict or
                        description not in self._switch_case_conversion):
                    raise ValueError(
                        "Description '{des}' must be unique and in "
                        "self._switch_case_conversion: {switch}.".format(
                            des=description,
                            switch=self._switch_case_conversion.keys()))
                if value is None or value == '':
                    value_dict[description] = value
                else:
                    value_dict[description] = self._switch_case_conversion[
                        description](value)
        except Exception as e:
            split = [(d, v) for d, v in zip_longest(
                self.columns_descriptions, line.split('\t'))]
            print("Error in line {l} of presentation file {f}. Line was:\n"
                  "{line}\nIt was splitted to:\n{split}\nException was: {e}"
                  "".format(l=line_number, f=self._presentation_file, e=e,
                            line=line, split=split))
            raise
        return False, value_dict

    def _parse_presentation_log(self):
        """Parse presentation log file and initialize class attributes."""
        with open(self._presentation_file) as f:
            self._log_content = [line.rstrip() for line in f.readlines()]

        # make sure we really got a paradigm file
        skip = self._test_paradigm_type()
        self.scenario = self._log_content[0].split('-')[-1].strip()
        self.time_str = self._log_content[1].split('-')[-1].strip().split(' ')[-1]
        hour, minute, second = self.time_str.split(':')
        month, day, year = self._log_content[1].split('-')[-1].strip().split(' ')[0].split('/')
        self.date_str = year + '.' + month + '.' + day
        self.start_time = time.mktime((int(year), int(month), int(day), int(hour), int(minute), int(second), 0, 0, 0))
        self.columns_descriptions = self._log_content[skip - 2].split('\t')
        for index, item in enumerate(self.columns_descriptions):
            if item == "Uncertainty":
                self.columns_descriptions[index] = self.columns_descriptions[index - 1] + ' ' + \
                                                   self.columns_descriptions[index]
        # if we find an empty line the second time, the standard presentation
        # file seems to be over, first
        nones = 0
        for i, line in enumerate(self._log_content[skip:]):
            exit_loop, onset = self._parse_line(line, i + skip)
            if onset is None:
                nones = nones + 1
                print(self._presentation_file, nones)
            if exit_loop or nones > 0:
                break
            if onset is not None:
                self._onset_list.append(onset)
        self.set_time_corrected_presentation()
        self.end_time = self.start_time + self.get_paradigm_duration()

    def get_presentation(self):
        """Create a tuple of all events in presentation log file."""
        return copy.deepcopy(self._onset_list)

    def set_time_corrected_presentation(self, event_number=0):
        """Set time of n-th event to 0.

        Correct the time stamp of all events accordingly.
        """
        if not self.get_presentation():
            return self.get_presentation()
        time_of_event = self.get_presentation()[event_number]['Time']
        if event_number != self._time_corrected_index:
            self._time_corrected_index = event_number
            self._time_corrected_onset_list = self.get_presentation()
            for item in self._time_corrected_onset_list:
                item['Time'] -= time_of_event
        return self.get_time_corrected_presentation()

    def get_time_corrected_presentation(self):
        """Get time corrected onset list."""
        return copy.deepcopy(self._time_corrected_onset_list)

    def get_paradigm_duration(self):
        """Get duration of paradigm."""
        paradigm = self.get_presentation()
        if not paradigm:
            return 0.0
        start_time = paradigm[0]['Time']
        event_end = start_time
        paradigm_end = start_time
        for event in paradigm:
            if ('Duration' in event and event['Duration'] is not None and
                    event['Duration'] != ''):
                event_end = event['Time'] + event['Duration']
            else:
                event_end = event['Time']
            paradigm_end = max(event_end, paradigm_end)
        return paradigm_end - start_time

    def _check_columns_descriptions(self, columns_descriptions, naming='Filter rule'):
        """Check, if columns_description is valid."""
        if isinstance(columns_descriptions, str):
            columns_descriptions = [columns_descriptions]
        for description in columns_descriptions:
            if description not in self.columns_descriptions or description == '':
                raise ValueError(naming, "must be one of:", self.columns_descriptions, "but a value of:", description,
                                 "was given.")
        return columns_descriptions

    def filter_time_corrected_presentation(self, filter_rules, time_offset_count=0, invert_filter=False):
        """Filter the time corrected onset list.

        :param filter_rules: dictionary {<Column_description_1>: <reg_exp>,
                                  <Column_description_2>: <reg_exp>,
                                  ...
                                  }
        returns a list of size of onset file, containing True,
        if all rules apply, False, if one or more rules fail.

        if invert_file = True, will return not(filter_vector)
        """
        filter_rules = copy.deepcopy(filter_rules)
        filter_vector = [False] * len(self.get_time_corrected_presentation())
        self._check_columns_descriptions(columns_descriptions=filter_rules, naming="Filter rules")
        for rule, value in filter_rules.items():
            filter_rules[rule] = re.compile(value)
        for index, item in enumerate(self.get_time_corrected_presentation()):
            match = True
            for rule, reg_exp in filter_rules.items():
                if item[rule] is None or not reg_exp.match(item[rule]):
                    match = False
                    break
            filter_vector[index] = match

        return [not i for i in filter_vector] if invert_filter else filter_vector

    def get_onset_list(self, columns_descriptions, filter_vector, description=False):
        """Get a list of columns for all rows, where filter_array = True.

        returns: [[<columns_description_1>,   <columns_descriptions_2>,  ...]  # if description == True
                  [<value_of_column_1_row_1>, <value_of_column_2_row_1>, ...],
                  [<value_of_column_1_row_2>, <value_of_column_2_row_1>, ...],
                  ...
                 ]
        """
        columns_descriptions = copy.deepcopy(
            self._check_columns_descriptions(columns_descriptions=columns_descriptions, naming='Columns descriptions'))
        if description:
            result_list = [columns_descriptions]
            result_index = 1
        else:
            result_list = []
            result_index = 0
        if not columns_descriptions:
            return result_list
        for index, event in enumerate(self.get_time_corrected_presentation()):
            if filter_vector[index]:
                result_list.append([])
                result_list[result_index] = []
                for description in columns_descriptions:
                    result_list[result_index].append(event[description])
                result_index += 1
        return result_list

    def get_response_times(self, filter_vector_event, filter_vector_response, event_name="",
                           only_first=True, only_same_trial=True, check_response_timing=False,
                           min_response_time=0.0, max_response_time=sys.float_info.max):
        """Response time between two event.

        filter_vector: [0, 0, 1, 0, 0, ...] indicates, where the event occurred.
                       use filter_time_corrected_presentation to acquire filter_vector.
        return [{'name': event_name, 'onset': Time, 'response_time': time_response_1 - time_event1,
                  'response_number': 1, 'duration': duration_event_1},
                {'name': event_name, 'onset': Time, 'response_time': time_response_2 - time_event1,
                'response_number': 2, 'duration': duration_event_1},
                {...},
                # if time_event_2 < time_response_n:
                {'name': event_name, 'onset': Time, 'response_time': time_response_n - time_event_2,
                 'response_number': 1, 'duration': duration_event_2},
                ...
               ]
        if only_first:
            return [{'name': event_name, onset: Time, 'response_time': time_response_2 - time_event_1,
                     'response_number': 1, 'duration': duration_event_1},
                     'name': event_name, onset: Time, 'response_time': time_response_2 - time_event_2,
                     'response_number': 1, 'duration': duration_event_2},
                    ...
                   ]
        """
        # TODO does not work as an intended, if filtered for "(UCS)|(CS_win)", to find only responses for ucs after cs_win, it only prints all UCS and does not save
        # todo the stimulus beforehand; it should care about the ucs-stimuli which are after the defined ones --> maybe new function? and overwork the current to work for everything else
        responsetime_list = []
        # store items to delete from onset list, may not be used in most cases
        clear_list = []
        found_start = False
        response_event_active = False
        for index, event in enumerate(self.get_time_corrected_presentation()):
            if filter_vector_event[index]:
                # event which triggers response was found, so we create a line in case no response to this event was found
                if response_event_active:
                    line = {'event_trial': event_trial, 'code': event_name, 'onset': start_time, 'duration': duration,
                            'response_trial': -99, 'response_time': -99,
                            'response_number': 0, 'response_timing': 'no response'}
                    responsetime_list.append(line)
                start_time = event['Time']
                duration = event['Duration']
                # event_name = (event_name if event_name else event['Code'])
                event_name = event['Code']
                event_trial = event['Trial']
                found_start = True
                """
                - set current event at status of a response_event
                - if this is a new event, which is no response_event, the former response_event will be written to the log
                - if a response is found, this variable will be deactivated
                """
                response_event_active = re.match(check_response_timing["response_stimulus_code"], event_name)
                response_number = 0
            if filter_vector_response[index] and found_start:
                response_time = event['Time'] - start_time
                response_trial = event['Trial']
                response_number += 1
                response_event_active = False
                if only_first:  # We need a new start event.
                    found_start = False
                if min_response_time < response_time < max_response_time:
                    line = {'event_trial': event_trial, 'code': event_name, 'onset': start_time, 'duration': duration,
                            'response_trial': response_trial, 'response_time': response_time,
                            'response_number': response_number}
                    # check for response timing:
                    if check_response_timing:
                        try:
                            pre_line = responsetime_list[-1]
                        except IndexError:  # happens only at first line
                            pre_line = line
                        # if response should only come with this event and not the one before (e.g.REWARD-Anticipation)

                        if (not re.match(check_response_timing["response_stimulus_code"], pre_line["code"]) or
                                not re.match(check_response_timing["response_stimulus_code"], line["code"])):
                            line["response_timing"] = "too fast"
                        elif only_same_trial and (response_trial > event_trial):
                            line["response_timing"] = "too slow"
                        else:
                            line["response_timing"] = "fine"

                        # todo: add "Stim Type" to line?

                        if not re.match(check_response_timing["response_stimulus_code"], line["code"]) and check_response_timing[
                            "clear_onset_list"]:
                            clear_list.append(line)

                    # if only_same_trial and response_trial == event_trial:
                    #     responsetime_list.append(line)
                    # elif not only_same_trial:
                    responsetime_list.append(line)


        for i in clear_list:
            # delete item from list which equals the item from the clear list
            del responsetime_list[responsetime_list.index(i)]

        return responsetime_list

    @staticmethod
    def _sort_multiple_lists(*lists):
        """Take multiple lists and sort all lists using the values in the first list."""
        zipped = zip(lists)
        zipped.sort()
        return zip(*zipped)

    def _sort_onset_dict(self, onset_dict):
        """Sort all events in a onset dict in time."""
        onset_dict = copy.deepcopy(onset_dict)
        for condition in onset_dict.keys():
            name = onset_dict[condition]['name']
            onset = onset_dict[condition]['onset']
            duration = onset_dict['condition']['duration']
            pmod = onset_dict['condition']['pmod']
            tmod = onset_dict['condition']['tmod']
            onset, duration, pmod, tmod = self._sort_multiple_lists(onset, duration, pmod, tmod)
            onset_dict[condition] = {'name': name, 'onset': onset, 'duration': duration,
                                     'pmod': pmod, 'tmod': tmod}
        return onset_dict

    @staticmethod
    def _add_onset_from_event(onset_dict, event, condition, duration=None, pmod=None, tmod=None):
        """Add an event to an onset dictionary.

        A condition = condition_1 will be appended to the appropiate dictionary.
        onset_dict = {condition_1: {'name': condition_1, 'onset' = [..., event['Time']_n], 'duration': [..., duration_n],
                                    'pmod': [..., pmod_n], 'tmod', [..., tmod_n]},
                      condition_2: {'name': condition_2, 'onset' = [...], 'duration': [...],
                                    'pmod': [...], 'tmod', [...]}}
        """
        onset = event['Time']
        if duration is None:
            duration = event['Duration']
        if condition not in onset_dict:
            onset_dict.update({condition: {'name': [condition], 'onset': [onset], 'duration': [duration],
                                           'pmod': [pmod], 'tmod': [tmod]}})
        else:
            onset_dict[condition]['name'] = condition
            onset_dict[condition]['onset'].append(onset)
            onset_dict[condition]['duration'].append(duration)
            onset_dict[condition]['pmod'].append(pmod)
            onset_dict[condition]['tmod'].append(tmod)
        return onset_dict

    def get_filtered_onset(self, condition, duration, filter_vector,
                           tmod=None, pmod=None, events=None, onset_dict=None):
        """Get an onset from timecorreted events."""
        if events is None:
            events = self.get_time_corrected_presentation()
        onset_dict = onset_dict or {}
        for index, event in enumerate(events):
            if filter_vector[index]:
                onset_dict = self._add_onset_from_event(onset_dict, event,
                                                        condition, duration,
                                                        pmod, tmod)
        return onset_dict

    @staticmethod
    def join_onsets(*onsets):
        """Join onsets for nipype."""
        onsets_new = {}
        for onset in onsets:
            for key in onset.keys():
                if key() not in onsets_new:
                    onsets_new.update({key: onset[key]})
                else:
                    raise ValueError("onset {0} already in {1}".format(key, onsets_new.keys()))
        return onsets_new

    @staticmethod
    def _make_list_of_length(target_length, source_list):
        """Make a list to be of length target_length.

        Raises an Error, if len(source_list) != 1 or len(source_list) != target_length).
        """
        if len(source_list) != 1 or len(source_list) != target_length:
            raise ValueError("Source list must be of length 1 or of length target_length.")
        if not isinstance(source_list, list):
            source_list = [source_list]
        return source_list * target_length

    @staticmethod
    def _number_duration_pmod_tmod_equals_number_onsets(onsets):
        """Change number of durations, pmods, tmods to match number of onsets."""
        if 'onsets' not in onsets:
            raise TypeError("Expecting a spm onsets file containing 'onsets'.")
        # len_onsets = len(onsets['onsets'])
        if 'tmod' in onsets:
            pass

    @staticmethod
    def merge_onset_type(max_time_difference, onset_name, duration, *onsets):
        """Merge multiple onset types in one onset type.

        max_time_difference: onsets with a smaller time difference then max_time_difference will be merged.
        onset_name: name of new merged onset.
        duration: duration of new merged onset. If duration is zero, the duration will be calculated as
        begin of the earliest onset and the end of the latest offset for a merged onset.
        *onsets: different onsets.
        """
        onsets_new = {}
        for index, onset in enumerate(onsets):
            for onset_second in onsets[index:]:
                pass
            for key in onset.keys():
                if key() not in onsets_new:
                    onsets_new.update({key: onset[key]})
                else:
                    raise ValueError("onset {0} already in {1}".format(key, onsets_new.keys()))
        return onsets_new

    def __str__(self):
        """Formated output of paths."""
        return '\n'.join(self._path_tuple)
