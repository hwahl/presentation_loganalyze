# -*- mode: python -*-

block_cipher = None


a = Analysis(['pla.py'],
             pathex=['H:\\Projekte\\presentation_loganalyze'],
             binaries=[],
             datas=[ ('.\\scripts\\gui\\gfx', 'gfx'), ('.\\json', 'json')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='pla',
          debug=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='pla')
